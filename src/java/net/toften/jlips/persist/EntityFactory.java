/*
* (C) 2002 toften.net
*
* Used code formatting convention: Sun
*
* Created by: thomas
*
* File change log:
* ----------------
* $Log: EntityFactory.java,v $
* Revision 1.6  2004/12/07 10:52:28  toften
* New init method for passing in a DBProperties object
*
* Revision 1.5  2004/11/17 19:30:10  toften
* Updated documentation
* Enabled the use of multiple persiste entities to one database table
*
* Revision 1.4  2004/10/30 12:24:19  toften
* Initial checkin
*
* Revision 1.3  2004/01/14 19:21:30  toften
* Various changes to make the code more coherent. Also much revised comments
*
* Revision 1.2  2003/01/07 23:24:35  toften
* Updated various comments.
* Added more support for Collections
*
* Revision 1.1  2002/12/16 14:48:23  toften
* Initial checkin
*
*
*/
package net.toften.jlips.persist;

import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import net.toften.jlips.persist.db.DBConnectionFactory;
import net.toften.jlips.persist.db.DBProperties;


/**
 * Java Lightweight Interface Persistence System (jlips) Factory.  This factory is
 * the main interface towards the application. It is responsible for creating
 * and finding objects specified by the interfaces.  To use the factory an
 * interface needs to be created, specifing the accessor methods for the
 * fields in the database table.  All methods in this factory returns an
 * object of the type Object. However this object can (and must) be typecasted
 * to the type of the interface specifying the accessor methods.
 *
 * @author thomas
 * @version $Revision: 1.6 $
 */
public final class EntityFactory {
    /**
     * String used to specify the name of the database connection handler
     * in the properties passed to jlips when it is initialised
     * 
     * @see net.toften.jlips.persist.db.DBConnectionHandler
     */
    public static final String EF_DB_CON_HAND  = "connection.handler";

    /**
     * String used to specify the name of the database connection properties
     * in the properties passed to jlips when it is initialised
     * 
     * @see net.toften.jlips.persist.db.DBProperties
     */
    public static final String EF_DB_PROP_HAND = "properties.handler";

    /**
     * String used to specify the name of the SQL statement creater class.
     * 
     * If this is not specified, the {@link net.toften.jlips.persist.db.DBDefaultStatement}
     * class will be used.
     * 
     * @see net.toften.jlips.persist.db.DBStatement
     */
    public static final String EF_DB_STM_HAND = "statement.handler";
    /**
     * Private default constructor to prevent instantiation of this
     * class
     * 
     */
    private EntityFactory(){
        
    }
    
    /**
     * Initializes the persistence system. This <b>must</b> be called 
     * before any of the methods are used.
     * 
     * To initialize the system you will need to supply information regardng
     * the connection to the database and the mapping of interfaces and
     * database tables. This is done in the two parameters that the <code>init</code>
     * method takes.
     * 
     * <h3>Database properties</h3>
     * The database connection properties <code>dbProps</code> takes two
     * properties. These properties are the full class path to the classes
     * that implements the connection information retrieval and the database connection
     * handler.
     * 
     * The <code>dbProps</code> parameter is a <code>Properties</code> Object
     * containing two elements. Example:
     * <pre>
     *  connection.handler=net.toften.jlips.persist.db.DBPooledConnection
     *  properties.handler=net.toften.jlips.persist.db.DBHardProperties
     * </pre>
     * The properties describe which classes jLips should use to manage
     * database connections and to retreive connection information.
     * When jLips initialise the database connection factory, it will instantiate
     * one object of each of these classes.
     * 
     * The <code>connection.handler</code> must implement the interface
     * {@link net.toften.jlips.persist.db.DBConnectionHandler} and the 
     * <code>properties.handler</code> must implement the interface
     * {@link net.toften.jlips.persist.db.DBProperties} interface.
     * 
     * <h3>Table map</h3>
     * DOCUMENT ME!
     *
     * @param dbProps Properties that describe the database connection details
     * @param tableMap Map describing the mapping between the persist entities 
     * and the database tables
     */
    public static void init(Properties dbProps, Map tableMap) {
        /*
         * The connection factory for generating connections to the database is initialised
         * 
         */
    	try {        	
            DBConnectionFactory.init(dbProps);
		} catch (Exception e) {
			e.printStackTrace(); // EXCEPTION
		}

        /*
         * The table handlers are initialised from the factory
         * The factory is given a map of the tables which is
         * used to create the table handlers
         */
		try {
            TableHandlerFactory.init(tableMap);
        } catch (Exception e) {
            e.printStackTrace(); // EXCEPTION
        }
    }

    public static void init(DBProperties dbProps, Map tableMap) {
        /*
         * The connection factory for generating connections to the database is initialised
         * 
         */
    	try {        	
            DBConnectionFactory.init(dbProps);
		} catch (Exception e) {
			e.printStackTrace(); // EXCEPTION
		}

        /*
         * The table handlers are initialised from the factory
         * The factory is given a map of the tables which is
         * used to create the table handlers
         */
		try {
            TableHandlerFactory.init(tableMap);
        } catch (Exception e) {
            e.printStackTrace(); // EXCEPTION
        }
    }

    /**
     * Creates a new row in the database. No values are inserted into the
     * database except the generated primary key.
     * 
     * This method is invoked after jLips has been initialised.
     * 
     * <pre>
     * Person p = (Person)EntityFactory.create(Person.class);
	 * p.setName("Thomas");
	 * p.commit();
	 * </pre>
	 * The code above will create a new record using the Person persist
	 * entity and set the field <code>name</code> to "Thomas".
	 * The last line commits the changes to the database.
     *
     * @param theEntity The class of the interface specifying the accessor
     *        methods
     *
     * @return The created object
     */
    public static Object create(Class theEntity) {
        return TableHandlerFactory.getHandler(theEntity).create(theEntity);
    }

    /**
     * Finds a row in the database given a primary key.
     *
     * @param theEntity The class of the interface specifying the accessor
     *        methods
     * @param key The primary key of the record to be found
     *
     * @return The found object. If the object could not be found null is
     * returned
     */
    public static Object find(Class theEntity, Object key) {
        return TableHandlerFactory.getHandler(theEntity).find(theEntity, key);
    }

    /**
     * Returns the name of the interface class given.  Note the returned name
     * is in lower case!
     * 
     * As defined for jLips, this must be the same name as the name
     * of the table in the database.
     *
     * @param theEntity The class of the interface specifying the accessor
     *        methods
     *
     * @return String containing the name of the interface in lower case
     */
    public static String getEntityName(Class theEntity) {
        return theEntity.getName()
                        .substring(theEntity.getName().lastIndexOf('.') + 1,
            theEntity.getName().length()).toLowerCase();
    }

    /**
     * Runs a predefined query. Queries are defined in the mapping interface
     * in the array <code>queries</code>. This array is a two dimentional array
     * in the following format:
     * <pre>
     *  public static final String queries[][] =
     *      {
     *          { "findByFirstName", "firstname = ?" },
     *          { "findByLastName", "lastname = ?" }
     *      };
     * </pre>
     * Where the first element is the name of the query passed to this method
     * in the query parameter. The second element is the WHERE part of the SQL
     * query. In this part every <code>?</code> will be replaces with the 
     * corosponding element in the <code>args</code> array, i.e. the first 
     * <code>?</code> will be replaced with <code>args[0]</code> and so forth.
     * 
     * The query will return a Collection that contains all the records that matches
     * the search. If no matches were found an empty Collection is returned. 
     *
     * @param theEntity The persist entity containing the search
     * @param query 
     * @param args Array of arguments to be passed into the query
     *
     * @return Collection of the found records
     * 
     * @see net.toften.jlips.persist.SearchCollectionEntityHandler
     */
    public static Collection search(Class theEntity, String query, Object[] args) {
        return TableHandlerFactory.getHandler(theEntity).getSearchCollection(theEntity,
            query, args);
    }
    
    /**
     * This method will return all the records in the table mapped to
     * the persist entity provided
     * 
     * @param theEntity The entity for which all records is requested
     * @return Collection of all records
     */
    public static Collection findAll(Class theEntity) {
        return TableHandlerFactory.getHandler(theEntity).findAll(theEntity);
    }
}
