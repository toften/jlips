/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * PrimaryKeyHandler.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: PrimaryKeyHandler.java,v $
 * Revision 1.2  2004/11/17 23:20:31  toften
 * Updated documentation
 *
 * Revision 1.1  2004/11/08 23:02:25  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist.db.pk;

import net.toften.jlips.persist.db.DBTableColumnInfo;
import net.toften.jlips.persist.db.DBTableInfo;

/**
 * Interface used for providing handlers for primary keys.
 * 
 * To separate the primary key from the Table handler, the primary
 * keys are handled by a separate class. This class must implement this
 * interface.
 * The PrimatyKeyHandler provides the TableHandler with a number of
 * services related to the primary key of a record
 * 
 * @author thomas
 *
 */
public interface PrimaryKeyHandler {
    /**
     * Returns the next available primary key for
     * creation of a new record
     * 
     * The method of generating a new unique primary key is up to
     * the individual primary key handler
     *
     * @return new unique primary key
     */
    public Object getNextPrimaryKey();
    
    /**
     * Returns the last used primary key
     * 
     * This might not have any meaning for certain types of primary
     * key types
     *
     * @return the last used primary key
     */
    public Object getLastPrimaryKey();
    
    /**
     * Return the table info for the table the primary key
     * handler is used by
     *
     * @return table info for handled table
     */
    public DBTableInfo getTableInfo();
    
    /**
     * Return the column info for the column that holds the
     * primary key
     *
     * @return
     */
    public DBTableColumnInfo getColumnInfo();
}
