/*
 * (C) 2003 toften.net
 *
 * DBTableInfo.java
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBTableInfo.java,v $
 * Revision 1.3  2004/12/07 10:49:35  toften
 * Now stores the data internally instead of accessing the database
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2003/11/02 17:49:14  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

import java.util.List;

/**
 * This class is used to return descriptions of a table in the database
 *
 * @author thomas
 */
public class DBTableInfo {
    /** DOCUMENT ME! */
    private String tableName;
    
    private List columnInfo;
    
    private int primaryKeyIndex;

    /**
     * Creates a new DBTableInfo object.
     *
     * @param tableName The name of the table this object describes
     */
    public DBTableInfo(String tableName) {
        this.tableName = tableName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getTableName() {
        return tableName;
    }
    
    public DBTableColumnInfo getPrimaryKeyColumnHandler() {
        return (DBTableColumnInfo)columnInfo.get(primaryKeyIndex);
    }
    
    public List getColumnHandlerList() {
        return columnInfo;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        return getTableName();
    }
    
    void setColumnInfo(List columns, int primaryKeyIndex){
        this.columnInfo 			= columns;
        this.primaryKeyIndex 	= primaryKeyIndex;
    }
}
