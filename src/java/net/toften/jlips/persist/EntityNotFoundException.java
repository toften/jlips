/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * EntityNotFoundException.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: EntityNotFoundException.java,v $
 * Revision 1.1  2004/11/08 22:58:18  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist;

/**
 * DOCUMENT ME!
 *
 * @author thomas
 *
 */
public class EntityNotFoundException extends RuntimeException {

    /**
     * Constructor for EntityNotFoundException
     * 
     * DOCUMENT ME!
     * 
     * @param arg0
     */
    public EntityNotFoundException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    }
}
