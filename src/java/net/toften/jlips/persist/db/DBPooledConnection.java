/*
 * (C) 2003 toften.net
 *
 * DBPooledConnection.java
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBPooledConnection.java,v $
 * Revision 1.5  2004/11/17 23:29:15  toften
 * Updated documentation
 *
 * Revision 1.4  2004/11/08 23:03:47  toften
 * Reorganised the Connection handler to use the abstract connection handlers as the base class
 *
 * Revision 1.3  2004/10/30 12:24:20  toften
 * Initial checkin
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2002/12/16 14:48:54  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.Vector;


/**
 * This connection handler implements a pool of connections that can 
 * be shared by all parts of jLips
 *
 * @author thomas
 * @version $Revision: 1.5 $
 */
public class DBPooledConnection extends DBAbstractConnection implements DBConnectionHandler {
    /** 
     * The maximum number of free connection in the pool
     */
    private static final int dbPoolMax = 5;

    /**
     * The connection pool   
     */
    private static Vector pool                = new Vector();

    /**
     * Will return a connection from the pool of connections
     * 
     * @see javax.sql.DataSource#getConnection()
     */
    public synchronized Connection getConnection() throws SQLException {
        Connection returnCon = null;

        if (connectionProperties == null) {
            throw new SQLException("Bad properties");
        }

        if (pool.size() == 0) {
            returnCon = DriverManager.getConnection(connectionProperties.getURL(),
                    connectionProperties.getUserName(),
                    connectionProperties.getPassword());
        } else {
            returnCon = (Connection)pool.get(0);
            pool.remove(0);
        }

        return returnCon;
    }

    /**
     * @see DBConnectionHandler#releaseConnection(Connection)
     */
    public synchronized void releaseConnection(Connection theConnection) {
        if (pool.size() < dbPoolMax) {
            pool.add(theConnection);
        }
    }
}
