/*
 * (C) 2003 toften.net
 *
 * DBDefaultStatement.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBDefaultStatement.java,v $
 * Revision 1.1  2004/12/07 10:32:55  toften
 * Initial checkin
 *
 *
 *
 */

package net.toften.jlips.persist.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



/**
 * SQL statement implementation using SQL standard notation
 * 
 * This class extends the {@link net.toften.jlips.persist.db.DBAbstractStatement}
 * class and overrides the specific SQL statements for the:
 * <ul>
 * <li>Select statement</li>
 * <li>Update statement</li>
 * <li>Insert statement</li>
 * <li>Delete statement</li>
 * </ul>
 *
 * This is the default implementation of the {@link net.toften.jlips.persist.db.DBStatement}
 * used by jLips if none is specified by the application.
 *
 * @author thomas
 * @version $Revision: 1.1 $
 */
public class DBDefaultStatement extends DBAbstractStatement implements DBStatement {
    
    /**
     * The resultSet returned by the statement
     */
    private ResultSet rs = null;
    
    /**
     * Will build a standard SQL select statement
     *
     * @return String containing the select statement
     * @throws DBBadPropertiesException if the provided properties are not correct or 
     * sufficient to create a select statement
     */
    protected String buildSelectStatement() throws DBBadPropertiesException {
        /*
         * Do initial tests
         */
        if (sourceTables.size() == 0)
            throw new DBBadPropertiesException("No source tables added yet");
        
        StringBuffer returnValue = new StringBuffer("select ");
    
        if (returnColumns.size() == 0) {
            returnValue.append("*");
        } else {
            appendColumnNames(returnValue, returnColumns.iterator());
        }
    
        returnValue.append(" from ");
    
        appendTableNames(returnValue, sourceTables.iterator());
    
        // Build the 'where' part
        if (expressions.size() > 0) {
            returnValue.append(" where ");
    
            appendStrings(returnValue, expressions.iterator(), " and ", "");
        }
        
        if (primaryKeyValue != null) {
            returnValue.append(expressions.size() > 0 ? " and " : " where ");
            appendPrimaryKeyEquals(returnValue);
        }
    
        returnValue.append(";");
        
        return returnValue.toString();
    }
    
    protected String buildUpdateStatement() throws DBBadPropertiesException {
        /*
         * Do initial tests
         */
        if (sourceTables.size() == 0)
            throw new DBBadPropertiesException("No source tables added yet");
        
        if (expressions.size() == 0)
            throw new DBBadPropertiesException("No new values provided");

        DBTableInfo table = (DBTableInfo)sourceTables.get(0);
        
        StringBuffer returnValue = new StringBuffer("update ");
        
        returnValue.append(table.getTableName());
        
        returnValue.append(" set ");
        if (expressions.size() > 0) {
            appendStrings(returnValue, expressions.iterator(), ", ", "");
        }
        
        // Build the 'where' part
        if (primaryKeyValue != null) {
            returnValue.append(" where ");
            appendPrimaryKeyEquals(returnValue);
        }
    
        returnValue.append(";");
        
        return returnValue.toString();
    }

    protected String buildInsertStatement() throws DBBadPropertiesException {
        /*
         * Do initial tests
         */
        if (sourceTables.size() == 0)
            throw new DBBadPropertiesException("No source table selected");
        
        if (primaryKeyValue == null)
            throw new DBBadPropertiesException("No new primary key value set");

        DBTableInfo table = (DBTableInfo)sourceTables.get(0);
        
        StringBuffer returnValue = new StringBuffer("insert into ");
        
        appendTableNames(returnValue, sourceTables.iterator());

        // Build the 'set primary key' part
        DBTableColumnInfo primaryKeyName = null;
        try {
            primaryKeyName = DBConnectionFactory.getPrimaryKeyField(table);
        } catch (SQLException e) {
            throw new DBBadPropertiesException("SQL Exception getting primary key info");
        }
        returnValue.append(" (" + primaryKeyName.getColumnName() + ") values ("
                + primaryKeyName.adjustQuotes(primaryKeyValue) + ")");
    
        returnValue.append(";");
        
        return returnValue.toString();        
    }
    
    protected String buildDeleteStatement() throws DBBadPropertiesException {
        /*
         * Do initial tests
         */
        if (sourceTables.size() == 0)
            throw new DBBadPropertiesException("No source table selected");

        DBTableInfo table = (DBTableInfo)sourceTables.get(0);
        
        StringBuffer returnValue = new StringBuffer("delete from ");
        
        returnValue.append(table.getTableName());
        
        // Build the 'where' part
        // Build the 'where' part
        if (expressions.size() > 0) {
            returnValue.append(" where ");
    
            appendStrings(returnValue, expressions.iterator(), " and ", "");
        }
        
        if (primaryKeyValue != null) {
            returnValue.append(expressions.size() > 0 ? " and " : " where ");
            appendPrimaryKeyEquals(returnValue);
        }
    
        returnValue.append(";");
        
        return returnValue.toString();
    }
    
    /* (non-Javadoc)
     * @see net.toften.jlips.persist.db.DBStatement#doSelect()
     */
    public ResultSet doSelect() throws SQLException, DBBadPropertiesException {
    	Statement s = prepareConnection();
    	
    	String query = buildSelectStatement();
    	
		rs = s.executeQuery(query);
		
		return rs;
    }
    
    /* (non-Javadoc)
     * @see net.toften.jlips.persist.db.DBStatement#doUpdate()
     */
    public ResultSet doUpdate() throws SQLException, DBBadPropertiesException {
    	Statement s = prepareConnection();
    	
    	String query = buildUpdateStatement();
    	
		rs = s.executeQuery(query);
		
		return rs;
    }
    
    /* (non-Javadoc)
     * @see net.toften.jlips.persist.db.DBStatement#doInsert()
     */
    public ResultSet doInsert() throws SQLException, DBBadPropertiesException {
    	Statement s = prepareConnection();
    	
    	String query = buildInsertStatement();
    	
		rs = s.executeQuery(query);
		
		return rs;
    }
    
    /* (non-Javadoc)
     * @see net.toften.jlips.persist.db.DBStatement#doDelete()
     */
    public ResultSet doDelete() throws SQLException, DBBadPropertiesException {
    	Statement s = prepareConnection();
    	
    	String query = buildDeleteStatement();
    	
		rs = s.executeQuery(query);
		
		return rs;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public void printStatement(String statementType) throws DBBadPropertiesException {
        String returnValue = null;
        

        if (statementType == DBS_SELECT_STM)
            returnValue = buildSelectStatement();
        if (statementType == DBS_UPDATE_STM)
            returnValue = buildUpdateStatement();
        if (statementType == DBS_DELETE_STM)
            returnValue = buildDeleteStatement();
        if (statementType == DBS_INSERT_STM)
            returnValue = buildInsertStatement();
        
        System.out.println(returnValue);
    }
}
