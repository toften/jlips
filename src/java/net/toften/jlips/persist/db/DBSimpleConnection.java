/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * DBSimpleConnection.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBSimpleConnection.java,v $
 * Revision 1.2  2004/11/17 23:33:39  toften
 * Updated documentation
 *
 * Revision 1.1  2004/11/08 23:02:54  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This connection handler that will provide a simple wrapper to the 
 * standard driver provided by the database driver
 * 
 * @author thomas
 *
 */
public class DBSimpleConnection extends DBAbstractConnection implements DBConnectionHandler {

    /**
     * @see javax.sql.DataSource#getConnection()
     */
    public Connection getConnection() throws SQLException {
        Connection returnCon = null;

        if (connectionProperties == null) {
            throw new SQLException("Bad properties");
        }

        returnCon = DriverManager.getConnection(connectionProperties.getURL(),
                connectionProperties.getUserName(),
                connectionProperties.getPassword());

        return returnCon;
    }
}