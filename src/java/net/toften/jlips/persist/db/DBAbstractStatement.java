/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * DBAbstractStatement.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBAbstractStatement.java,v $
 * Revision 1.1  2004/12/07 10:33:45  toften
 * Created as base class for statement managers
 *
 */

package net.toften.jlips.persist.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * SQL statement which direct implementation of the {@link net.toften.jlips.persist.db.DBStatement}
 * interface can extend.
 * 
 * The class implements a set of helper method designed to assist in generating the
 * SQL statements for the database request
 *
 * @author thomas
 *
 */
public abstract class DBAbstractStatement {
    /**
     * Holds the expressions passed in by jLips
     * 
     * @see #addEqualExpression(DBTableColumnInfo, Object)
     */
    protected List expressions = new Vector();
    
    /**
     * Holds the database connection
     * 
     * @see #prepareConnection()
     */
    protected Connection myCon = null;
    
    /**
     * Holder for valued to be changed by the statement
     * 
     * @see #addSetValue(DBTableColumnInfo, Object)
     */
    protected Object primaryKeyValue = null;
    
    /**
     * Holds the return columns requested by jLips
     * 
     * @see #addReturnColumn(DBTableColumnInfo)
     */
    protected List returnColumns = new Vector();

    /**
     * Holds the source tables requested by jLips
     * 
     * @see #addSourceTable(DBTableInfo)
     * @see #addReturnColumn(DBTableColumnInfo)
     */
    protected List sourceTables = new Vector();

    /**
     * @see DBStatement#addEqualExpression(DBTableColumnInfo, Object)
     */
    public void addEqualExpression(DBTableColumnInfo newColumn, Object value) {
        expressions.add(newColumn.getEqualString(value));
    }

    /**
     * @see DBStatement#addReturnColumn(DBTableColumnInfo)
     */
    public void addReturnColumn(DBTableColumnInfo newReturnColumn) {
        returnColumns.add(newReturnColumn);
    }

    /**
     * @see DBStatement#addSourceTable(DBTableInfo)
     */
    public void addSourceTable(DBTableInfo newTable) {
        sourceTables.add(newTable);
    }

    /**
     * @see DBStatement#setPrimaryKeyValue(Object)
     */
    public void setPrimaryKeyValue(Object pKey) {
        primaryKeyValue = pKey;
    }
    /**
     * Appends column names to a StringBuffer using ", " as delimeter
     *
     * The column names are taken from an Iterator that must iterate over a 
     * Collection containing DBTableColumnInfo objects
     * 
     * @param s StringBuffer to append column names to
     * @param iter Iterator for Collection containing DBTableColumnInfo objects
     * 
     * @see DBTableColumnInfo
     */
    protected void appendColumnNames(StringBuffer s, Iterator iter) {
    	for (; iter.hasNext();) {
    		s.append(((DBTableColumnInfo)iter.next()).getColumnName());
    		s.append(iter.hasNext() ? ", " : "");
    	}
    }

    /**
     * Appends a list of strings to a StringBuffer using a defined delimiter.
     * 
     * The strings are taken from an Iterator that must iterate over a 
     * Collection containing String objects
     * 
     * The delimeter will be applied between the strings, but not in the end
     *
     * @param s StringBuffer to append strings to
     * @param iter Iterator for Collection containing String objects
     * @param delimiter The delimeter used between the strings
     * @param preString String appended before every string
     */
    protected void appendStrings(StringBuffer s, Iterator iter, String delimiter, String preString) {
        for (; iter.hasNext();) {
            s.append(preString);
            s.append((String)iter.next());
            s.append(iter.hasNext() ? delimiter : "");
        }
    }

    /**
     * Appends table names to a StringBuffer using ", " as delimeter
     *
     * The table names are taken from an Iterator that must iterate over a 
     * Collection containing DBTableInfo objects
     * 
     * @param s StringBuffer to append table names to
     * @param iter Iterator for Collection containing DBTableInfo objects
     * 
     * @see DBTableInfo
     */
    protected void appendTableNames(StringBuffer s, Iterator iter) {
        for (; iter.hasNext();) {
            s.append(((DBTableInfo)iter.next()).getTableName());
            s.append(iter.hasNext() ? ", " : "");
        }
    }
    
    /**
     * Will append a equal expression for the primary key for the first
     * added source table
     * 
     * This method will append an equal expression to the provided StringBuffer
     * using the specified primary key value and the name of the primary
     * key in the first source table added.
     * 
     * For example if the first source table added was <code>person</code>:
     * <pre>
     * 	dbs.addSourceTable(personTableInfo);
     * </pre>
     * and the primary key name of the <code>person</code> table is <code>id</code>
     * and the type of the primary key is <code>Integer</code> then the added expression is
     * <pre>
     * 	id = 1
     * </pre>
     * This is of course if the provided primary key value is 1:
     * <pre>
     * 	dbs.setPrimaryKeyValue(new Integer(1));
     * </pre>
     *
     * @param s StringBuffer to append the equal string to
     * @throws DBBadPropertiesException
     */
    protected void appendPrimaryKeyEquals(StringBuffer s) throws DBBadPropertiesException {
        if (sourceTables.size() > 0) {
	        DBTableInfo table = (DBTableInfo)sourceTables.get(0);
	        
	        if (primaryKeyValue != null) {
		        DBTableColumnInfo primaryKeyName = null;
		        
		        try {
		            primaryKeyName = DBConnectionFactory.getPrimaryKeyField(table);
		        } catch (SQLException e) {
		            throw new DBBadPropertiesException("SQL Exception getting primary key info");
		        }
		        
		        s.append(primaryKeyName.getEqualString(primaryKeyValue));
	        }
	        else
	            throw new DBBadPropertiesException("No primary key value provided");
        }
        else
            throw new DBBadPropertiesException("No source tables added yet");

    }

    /**
     * @see DBStatement#release()
     */
    public void release() {
    	if (myCon != null) {
    		DBConnectionFactory.releaseConnection(myCon);
    	}
    }

    /**
     * Will request a database connection from the database connection
     * factory and create a statement
     *
     * @throws SQLException passed from the connection factory
     * 
     * @see DBConnectionFactory#getConnection()
     * @see #s
     */
    protected Statement prepareConnection() throws SQLException {
    	myCon = DBConnectionFactory.getConnection();
    
    	return myCon.createStatement();		
    }

    /**
     * Returns the provided data for the statement
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
    	StringBuffer returnValue = new StringBuffer();
        
    	returnValue.append("Source tables: ");
        appendTableNames(returnValue, sourceTables.iterator());

    	returnValue.append(" | Return columns: ");
        appendColumnNames(returnValue, returnColumns.iterator());
    
    	returnValue.append(" | Equal expressions: ");
        appendStrings(returnValue, expressions.iterator(), ", ", "");
        
        returnValue.append(myCon == null ? " | No connection created" : " | Connection created");
    	
        return returnValue.toString();
    }
}
