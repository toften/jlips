/*
 * (C) 2004 toften.net
 *
 * DBAbstractConnection.java
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBAbstractConnection.java,v $
 * Revision 1.4  2004/11/17 23:51:29  toften
 * Updated documentation
 *
 * Revision 1.3  2004/11/17 19:55:21  toften
 * Removed implemented interface - should be specified in the concrete class
 *
 * Revision 1.2  2004/11/17 19:52:13  toften
 * Updated documentation
 *
 * Revision 1.1  2004/11/08 23:02:54  toften
 * Initial checkin
 *
 *
 *
 */

package net.toften.jlips.persist.db;

import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Provide an abstract base class for connection handlers
 * 
 * The class will implement the {@link javax.sql.DataSource} interface
 *
 * @author thomas
 * @version $Revision: 1.4 $
 */
public abstract class DBAbstractConnection {
    /** 
     * Holds the Properties provided by the connection factory
     *
     * @see DBConnectionFactory#init(Map)
     */
    protected DBProperties connectionProperties = null;

    /**
     * This is not supported by this handler
     * 
     * @see javax.sql.DataSource#getConnection(java.lang.String,
     *      java.lang.String)
     */
    public Connection getConnection(String username, String password)
        throws SQLException {
       
        throw new SQLException("Operation getConnection is not supported!");
    }

    /**
     * @see javax.sql.DataSource#getLogWriter()
     */
    public PrintWriter getLogWriter() throws SQLException {
        return DriverManager.getLogWriter();
    }

    /**
     * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
     */
    public void setLogWriter(PrintWriter out) throws SQLException {
        DriverManager.setLogWriter(out);
    }

    /**
     * @see javax.sql.DataSource#setLoginTimeout(int)
     */
    public void setLoginTimeout(int seconds) throws SQLException {
        DriverManager.setLoginTimeout(seconds);
    }

    /**
     * @see javax.sql.DataSource#getLoginTimeout()
     */
    public int getLoginTimeout() throws SQLException {
        return DriverManager.getLoginTimeout();
    }

    /**
     * This method loads the driver for the database
     * 
     * @see DBConnectionHandler#setConnectionProperties(DBProperties)
     */
    public void setConnectionProperties(DBProperties connectionProperties) 
        throws DBBadPropertiesException {
        
        this.connectionProperties = connectionProperties;

        try {
            Class.forName(connectionProperties.getDriver()).newInstance();
        } catch (InstantiationException e) {
            throw new DBBadPropertiesException("InstantiationException");
        } catch (IllegalAccessException e) {
            throw new DBBadPropertiesException("IllegalAccessException");
        } catch (ClassNotFoundException e) {
            throw new DBBadPropertiesException("ClassNotFoundException");
        }
    }

    /**
     * @see DBConnectionHandler#getConnectionProperties()
     */
    public DBProperties getConnectionProperties() {
        return connectionProperties;
    }
    /**
     * @see DBConnectionHandler#releaseConnection(Connection)
     */
    public synchronized void releaseConnection(Connection theConnection) {
    }
}
