/*
 * (C) 2003 toften.net
 *
 * AbstractCollectionEntityHandler.java
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: AbstractCollectionEntityHandler.java,v $
 * Revision 1.6  2004/11/17 19:01:46  toften
 * Changed a Vector to a List
 *
 * Revision 1.5  2004/11/17 18:54:47  toften
 * Updated documentation
 * Enabled the use of multiple persiste entities to one database table
 *
 * Revision 1.4  2004/10/30 12:24:19  toften
 * Initial checkin
 *
 * Revision 1.3  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.2  2003/01/07 23:24:35  toften
 * Updated various comments.
 * Added more support for Collections
 *
 * Revision 1.1  2002/12/16 14:48:26  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Vector;


/**
 * This class is the base class for any collection type class, 
 * which needs to hold a list of entities.
 * 
 * This entities in the list <b>must</b> all be of the same type,
 * i.e. they must have the same entity class and the same
 * {@link net.toften.jlips.persist.TableHandler}
 *
 * @author thomas
 * @version $Revision: 1.6 $
 */
abstract class AbstractCollectionEntityHandler implements Collection {
    /**
     * The tablehandler of the table the object is a collection
     * of.
     * Whenever the collection needs to access the table in the
     * database it must go through this tablehandler.
     */
    protected TableHandler collectedTableHandler;
    
    /**
     * This is the persist entity of the colleced records.
     * 
     * When a record is returned it will be returned as this type.
     * 
     * @see RecordsIterator
     */
    protected Class collectedEntity; 

    /** 
     * This vector holds the primary keys for the records in the
     * collection.
     * 
     * When a record is requested, the record is loaded from the
     * table.
     * 
     * @see RecordsIterator.next()
     */
    protected List records = new Vector();

    /**
     * Creates a new AbstractCollectionEntityHandler object.
     *
     * @param collectedTableHandler The table handler for the colleced reords
     * @param collectedEntity The persist entity type the collected records should be
     * returned as by the {@link RecordsIterator}
     * 
     * @see TableHandler
     */
    AbstractCollectionEntityHandler(TableHandler collectedTableHandler, Class collectedEntity) {
        this.collectedTableHandler            = collectedTableHandler;
        this.collectedEntity					= collectedEntity;
    }

    /**
     * @see Collection#add(E)
     */
    public boolean add(Object o) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see java.util.Collection#addAll(java.util.Collection)
     */
    public boolean addAll(Collection c) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see java.util.Collection#clear()
     */
    public void clear() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see java.util.Collection#contains(java.lang.Object)
     */
    public boolean contains(Object o) throws NullPointerException {
        if (o != null) {
            return records.contains(o);
        } else {
            throw new NullPointerException();
        }
    }

    /**
     * @see java.util.Collection#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection c) throws NullPointerException {
        if (c != null) {
            return records.containsAll(c);
        } else {
            throw new NullPointerException();
        }
    }

    /**
     * @see java.util.Collection#isEmpty()
     */
    public boolean isEmpty() {
        return records.isEmpty();
    }

    /**
     * @see java.util.Collection#iterator()
     */
    public Iterator iterator() {
        return new RecordsIterator();
    }

    /**
     * @see java.util.Collection#remove(java.lang.Object)
     */
    public boolean remove(Object o) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see java.util.Collection#removeAll(java.util.Collection)
     */
    public boolean removeAll(Collection c) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see java.util.Collection#retainAll(java.util.Collection)
     */
    public boolean retainAll(Collection c) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see java.util.Collection#size()
     */
    public int size() {
        return records.size();
    }

    /**
     * @see java.util.Collection#toArray()
     */
    public Object[] toArray() {
        return records.toArray();
    }

    /**
     * @see java.util.Collection#toArray(java.lang.Object)
     */
    public Object[] toArray(Object[] a) throws NullPointerException {
        if (a != null) {
            return records.toArray(a);
        } else {
            throw new NullPointerException();
        }
    }

    /**
     * This iterator will iterator over the records in the collection
     *
     * @author thomas
     * @version $Revision: 1.6 $
     */
    public class RecordsIterator implements Iterator {
        /**
         *  This iterator used a wrapping technique
         *
         * @see AbstractCollectionEntityHandler#records
         */
        private Iterator wrappedIterator;

        /**
         * Creates a new RecordsIterator object.
         */
        public RecordsIterator() {
            wrappedIterator = records.iterator();
        }

        /**
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {
            return wrappedIterator.hasNext();
        }

        /**
         * This method returns the next record in the collection
         * 
         * @see java.util.Iterator#next()
         */
        public Object next() throws NoSuchElementException {
            return collectedTableHandler.find(collectedEntity, wrappedIterator.next()); // EntityFactory.find(th.getEntity(), localIter.next());
        }

        /**
         * @see java.util.Iterator#remove()
         */
        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }
    }
}
