/*
 * (C) 2003 toften.net
 *
 * DBProperties.java
 *
 * Copyright (C) 2004 toften.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBProperties.java,v $
 * Revision 1.4  2004/11/17 19:31:19  toften
 * Updated documentation
 *
 * Revision 1.3  2004/10/30 12:24:20  toften
 * Initial checkin
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2002/12/16 14:48:32  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

import java.util.Map;


/**
 * Interface for the class that jLips uses to get information about how to connect
 * to the database.
 * 
 * jLips has it's own {@link net.toften.jlips.persist.db.DBConnectionFactory Factory}
 * that is used to create connections to the database whenever jLips needs them.
 * This Factory must know how to connect to the database and the application must
 * provide a class that will return this information.
 * 
 * The actual connections are handled by a class implementing the 
 * {@link net.toften.jlips.persist.db.DBConnectionHandler} interface. jLips provides
 * a number of these classes.
 *
 * @author thomas
 * @version $Revision: 1.4 $
 * 
 * @see net.toften.jlips.persist.db.DBAbstractConnection
 */
public interface DBProperties {
    /**
     * This method must return the schema that is used by the application
     * 
     * A schema is a text string that indicates which 'part' of the database is used
     * by the application using jLips
     *
     * @return The name of the database schema used by the application
     */
    public String getSchema();

    /**
     * This method must return the URL that jLips uses to connect to
     * the database
     * 
     * The URL to a database takes the general  form  <i>jdbc:subprotocol:subname</i>
     *
     * @return The URL used to connect to the database
     */
    public String getURL();

    /**
     * Must return the username used to connect to the database with
     *
     * @return The username used to access the database
     */
    public String getUserName();

    /**
     * The driver the Factory must load to connect to the database
     *
     * @return The full classpath to the database JDBC driver
     */
    public String getDriver();

    /**
     * The password used to connect to the database
     *
     * @return The password used to access the database
     */
    public String getPassword();
    
    /**
     * When the Factory is initialised this method is invoked passing in
     * the database properties supplied by the application
     * 
     * When the application initialises jLips it must provide a set of
     * properties specifying how jLips connects to the database. One of these
     * properties is the class implementing this interface.
     * The application might also provide other information that is relevant to
     * the class implementing this interface.
     * When implementing this method, the class implementing this interface gets
     * a chance to interrogate the properties passed in by the application. 
     *
     * @param dbInitProbs Database properties passed in by the application
     */
    public void setDBInitProperties(Map dbInitProbs);
}
