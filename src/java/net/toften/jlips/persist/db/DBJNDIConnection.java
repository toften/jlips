/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * DBJNDIConnection.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBJNDIConnection.java,v $
 * Revision 1.3  2004/11/17 19:42:10  toften
 * Updated documentation
 *
 * Revision 1.2  2004/11/08 23:03:47  toften
 * Reorganised the Connection handler to use the abstract connection handlers as the base class
 *
 * Revision 1.1  2004/10/30 12:24:20  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * Connection handler for a JNDI connection
 * 
 * When using this connection handler, the URI of the JNDI database must be passed
 * in by the application.
 * This is done usign the database properties object passed in through the
 * {@link net.toften.jlips.persist.EntityFactory#init(Properties, Map) method
 * 
 * @author thomas
 * 
 * @see net.toften.jlips.persist.db.DBJNDIProperties
 */
public class DBJNDIConnection extends DBWrappedConnection implements DBConnectionHandler {
    /**
     * @see net.toften.jlips.persist.db.DBConnectionHandler#setConnectionProperties(net.toften.jlips.persist.db.DBProperties)
     */
    public void setConnectionProperties(DBProperties connectionProperties)
            throws DBBadPropertiesException {
        // TODO Auto-generated method stub
        String jndi_uri = connectionProperties.getURL();
        Context ctx;

        try {
            ctx = new InitialContext();
            wrappedDS = (DataSource)ctx.lookup(jndi_uri);
            //System.out.println("Got " + wrappedDS.toString());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
