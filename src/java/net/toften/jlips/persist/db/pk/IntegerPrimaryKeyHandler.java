/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * IntegerPKH.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: IntegerPrimaryKeyHandler.java,v $
 * Revision 1.2  2004/11/17 23:20:31  toften
 * Updated documentation
 *
 * Revision 1.1  2004/11/08 23:02:25  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist.db.pk;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import net.toften.jlips.persist.db.DBConnectionFactory;
import net.toften.jlips.persist.db.DBTableColumnInfo;
import net.toften.jlips.persist.db.DBTableInfo;

/**
 * This is a primary key handler for a primary key of
 * the type Integer
 * 
 * For info about how the key is generated, see
 * {@link #getNextPrimaryKey()}
 *
 * @author thomas
 */
public class IntegerPrimaryKeyHandler implements PrimaryKeyHandler {
    /**
     * Holder for the last used integer primary key
     */
    private Integer lastPrimaryKey = null;
    
    /**
     * Holder for the table info this primary key handler is
     * related to
     */
    private DBTableInfo primaryKeyTableInfo = null;
    
    /**
     * Holder for the column info that is the column containing
     * the primary key
     */
    private DBTableColumnInfo primaryKeyColumnInfo = null;


    /**
     * Constructor for IntegerPrimaryKeyHandler
     * 
     * @param tableInfo the table info for the related table
     * @param columnInfo the column info for the primary key column
     */
    public IntegerPrimaryKeyHandler(DBTableInfo tableInfo, DBTableColumnInfo columnInfo) {
        // Setup the local copies
        this.primaryKeyTableInfo 	= tableInfo;
        this.primaryKeyColumnInfo 	= columnInfo;
        
        /*
         * Start out by finding the last primary key used in the table
         */
        Connection myCon = null;

        try {
            myCon = DBConnectionFactory.getConnection();

            Statement s = myCon.createStatement();

            // TODO use DBStatement
            ResultSet rs = s.executeQuery("select max(" +
                    columnInfo.getColumnName() + ") from " + tableInfo.getTableName());

            while (rs.next() == true) {
                lastPrimaryKey = Integer.valueOf(rs.getInt("max(" + columnInfo.getColumnName() + ")"));

                if (lastPrimaryKey == null) {
                    lastPrimaryKey = new Integer(0);
                }

                break; // Only take the first row!!
            }
        } catch (Exception e) {
            System.out.println(e.toString()); // EXCEPTION
        } finally {
            DBConnectionFactory.releaseConnection(myCon);
        }
    }
    
    /**
     * The way this primary key handler generates a new primary
	 * key is by using the highest number currently in the database
	 * and then incrementing it
	 * 
     * @see net.toften.jlips.persist.db.pk.PrimaryKeyHandler#getNextPrimaryKey()
     */
    public Object getNextPrimaryKey() {
        lastPrimaryKey  = new Integer(lastPrimaryKey.intValue() + 1);
        
        return getLastPrimaryKey();
    }

    /**
     * @see net.toften.jlips.persist.db.pk.PrimaryKeyHandler#getLastPrimaryKey()
     */
    public Object getLastPrimaryKey() {
        return lastPrimaryKey;
    }

    /**
     * @see net.toften.jlips.persist.db.pk.PrimaryKeyHandler#getColumnInfo()
     */
    public DBTableColumnInfo getColumnInfo() {
        return primaryKeyColumnInfo;
    }
    /**
     * @see net.toften.jlips.persist.db.pk.PrimaryKeyHandler#getTableInfo()
     */
    public DBTableInfo getTableInfo() {
        return primaryKeyTableInfo;
    }
}
