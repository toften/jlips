/*
 * (C) 2003 toften.net
 *
 * DBConnectionHandler.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBConnectionHandler.java,v $
 * Revision 1.4  2004/11/17 19:31:19  toften
 * Updated documentation
 *
 * Revision 1.3  2004/11/08 23:03:47  toften
 * Reorganised the Connection handler to use the abstract connection handlers as the base class
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2002/12/16 14:48:50  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

import java.sql.Connection;

import javax.sql.DataSource;


/**
 * This interface must be implemented by the class handling the connection
 * to the database.
 * 
 * The responsibility of the connction handler is to create and release connections
 * to the database. The interface extends the {@link javax.sql.DataSource} interface
 * and must thus implement it's methods aswell.
 * jLips provides a number of connection handlers aswell as an 
 * {@link net.toften.jlips.persist.db.DBAbstractConnection abstract} connection handler 
 * and a DataSource {@link net.toften.jlips.persist.db.DBWrappedConnection wrapper}
 * connection handler for the use of the application.
 * 
 * Normally the application would use the {@link net.toften.jlips.persist.db.DBSimpleConnection}
 * connection handler.
 *
 * @author thomas
 * @version $Revision: 1.4 $
 * 
 * @see javax.sql.DataSource
 */
public interface DBConnectionHandler extends DataSource {
    /**
     * Method used to pass in the database connection properties provided
     * by the application
     * 
     * The connection handler will need these properties to connect to the 
     * database
     *
     * @param connectionProperties The database connection properties
     * passed in bu the application
     *
     * @throws DBBadPropertiesException Should be thrown if the passed in 
     * properties are not valid or does not contain all the necessary information
     */
    public void setConnectionProperties(DBProperties connectionProperties)
        throws DBBadPropertiesException;

    /**
     * When jLips is done with a connection it will call this method to 
     * allow the connection handler to properly dispose of the connection
     *
     * @param theConnection The connection to be disposed
     */
    public void releaseConnection(Connection theConnection);
    
    /**
     * This method must return a reference to the properties passed in by the application.
     * 
     * Because the properties might be changed by the connection handler, the Factory does
     * not hold a copy of these properties.
     * Whenever the Factory needs to access the properties it will use this method.
     *
     * @return The current DBProperties object
     */
    public DBProperties getConnectionProperties();
}
