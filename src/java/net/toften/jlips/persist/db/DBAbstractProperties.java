/*
 * (C) 2003 toften.net
 *
 * DBAbstractProperties.java
 *
 * Copyright (C) 2004 toften.net
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBAbstractProperties.java,v $
 * Revision 1.4  2004/11/17 19:52:41  toften
 * Updated documentation
 *
 * Revision 1.3  2004/10/30 12:24:20  toften
 * Initial checkin
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2002/12/16 14:48:38  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

import java.util.Map;
import java.util.Properties;


/**
 * Abstract helper class implementing the methods defined in the 
 * {@link net.toften.jlips.persist.db.DBProperties} class.
 * 
 * 
 *
 * @author thomas
 * @version $Revision: 1.4 $
 */
public abstract class DBAbstractProperties {
    /**
     * These properties are used by this class to hold the 
     * database properties.
     * 
     * When the class is extended, these Properties should be
     * initialise appropriately using the identifying strings.
     * 
     * @see DBAbstractProperties#DB_DRIVER
     * @see DBAbstractProperties#DB_PASSWORD
     * @see DBAbstractProperties#DB_SCHEMA
     * @see DBAbstractProperties#DB_URL
     * @see DBAbstractProperties#DB_USERNAME
     * @see DBAbstractProperties#DBAbstractProperties(String, String, String, String, String)
     */
    protected Properties dbProp = new Properties();
    
    /**
     * This is initialised to the database properties provided by the application
     * when jLips were initialised
     * 
     * @see DBProperties#setDBInitProperties(Map)
     */
    protected Properties dbInitProb = null;

    /** String used to identify the driver class */
    protected static final String DB_DRIVER   = "connection.driver";

    /** String used to identify the url */
    protected static final String DB_URL      = "connection.url";

    /** String used to identify the schema */
    protected static final String DB_SCHEMA   = "schema";

    /** String used to identify the username */
    protected static final String DB_USERNAME = "user";

    /** String used to identify the password */
    protected static final String DB_PASSWORD = "password";

    /**
     * Default constructor for DBAbstractProperties
     * 
     * Is provided because of the other constructors for this class.
     * Method doesn't do anything!
     */
    public DBAbstractProperties() {
        
    }
    
    /**
     * Constructor for DBAbstractProperties
     * 
     * This constructor will take all the database connection parameters as 
     * arguments and initialise the {@link DBAbstractProperties#dbProp internal properties}
     * class 
     * 
     * @param driver
     * @param url
     * @param username
     * @param password
     * @param schema
     */
    public DBAbstractProperties(String driver, String url, String username, String password, String schema) {
        dbProp.put(DB_DRIVER, driver);
        dbProp.put(DB_URL, url);
        dbProp.put(DB_USERNAME, username);
        dbProp.put(DB_PASSWORD, password);
        dbProp.put(DB_SCHEMA, schema);
    }
    
    /**
     * @see net.toften.jlips.persist.db.DBProperties#getDriver()
     */
    public String getDriver() {
        return dbProp.getProperty(DBAbstractProperties.DB_DRIVER);
    }

    /**
     * @see net.toften.jlips.persist.db.DBProperties#getPassword()
     */
    public String getPassword() {
        return dbProp.getProperty(DBAbstractProperties.DB_PASSWORD);
    }

    /**
     * @see net.toften.jlips.persist.db.DBProperties#getSchema()
     */
    public String getSchema() {
        return dbProp.getProperty(DBAbstractProperties.DB_SCHEMA);
    }

    /**
     * @see net.toften.jlips.persist.db.DBProperties#getURL()
     */
    public String getURL() {
        return dbProp.getProperty(DBAbstractProperties.DB_URL);
    }

    /**
     * @see net.toften.jlips.persist.db.DBProperties#getUserName()
     */
    public String getUserName() {
        return dbProp.getProperty(DBAbstractProperties.DB_USERNAME);
    }
    
    /**
     * This method stores a {@link DBAbstractProperties#dbInitProb reference} to the 
     * initialisation properties given to jLips when it was initialised by the application.
     * 
     * This method will be invoked by the {@link DBConnectionFactory#init(Map)} method
     *
     * @param dbInitProbs The database properties provided by the application
     * 
     * @see net.toften.jlips.persist.EntityFactory#init(Properties, Map)
     * @see DBProperties#setDBInitProperties(Map)
     */
    public void setDBInitProperties(Map dbInitProbs) {
        this.dbInitProb = (Properties)dbInitProbs;
    }

}
