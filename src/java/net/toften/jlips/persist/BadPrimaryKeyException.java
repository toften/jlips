/*
 * Created on 18-Nov-2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package net.toften.jlips.persist;

/**
 * @author thomas
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class BadPrimaryKeyException extends Exception {

	/**
	 * @param arg0
	 */
	public BadPrimaryKeyException(String arg0) {
		super(arg0);
	}
}
