/*
 * (C) 2003 toften.net
 * 
 * DBConnectionFactory.java
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * 
 * 
 * Used code formatting convention: Sun
 * 
 * Created by: thomas
 * 
 * File change log:
 * ----------------
 * $Log: DBConnectionFactory.java,v $
 * Revision 1.6  2004/12/07 10:47:28  toften
 * Added support for initialisation with DBProperties object
 * Now stores the database meta data instead of fetching if every time it is requested by jLips
 *
 * Revision 1.5  2004/11/17 19:38:24  toften
 * Updated documentation
 *
 * Revision 1.4  2004/11/08 23:03:47  toften
 * Reorganised the Connection handler to use the abstract connection handlers as the base class
 *
 * Revision 1.3  2004/10/30 12:24:20  toften Initial checkin
 * 
 * Revision 1.2 2004/01/14 19:21:30 toften Various changes to make the code more
 * coherent. Also much revised comments
 * 
 * Revision 1.1 2002/12/16 14:48:53 toften Initial checkin
 * 
 *  
 */

package net.toften.jlips.persist.db;

import java.util.List;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import net.toften.jlips.persist.EntityFactory;

/**
 * This class act as a factory for database connections. Because
 * 
 * @author thomas
 * @version $Revision: 1.6 $
 */
public final class DBConnectionFactory {
    /**
     * The DEFAULT_CON string contains the name of the class that will be used
     * as the default connection handler if no other handler is specified when
     * jLips is initialised
     * 
     * @see net.toften.jlips.persist.EntityFactory#init(Properties, Map)
     * @see DBSimpleConnection
     */
    private static final String DEFAULT_CON = "net.toften.jlips.persist.db.DBSimpleConnection";
    
    private static final String DEFAULT_STM = "net.toften.jlips.persist.db.DBDefaultStatement";

    /**
     * Holder for the database connection handler object
     * 
     * @see DBPooledConnection
     */
    private static DBConnectionHandler connectionHandler = null;

    private static Class sqlStatementHandler = null;
    
    private static List tableList = null;
    
    /**
     * This method initialises the Database Connection Factory
     * 
     * The method will validate, that all parameters has been supplied are correct.
     * The method will load an instantiate the supplied database properties
     *
     * @param dbInitProp The database properties that was supplied
     * 
     * @throws ClassNotFoundException Thrown if the classes specified in the database properties 
     * could not be loaded or instantiated
     * @throws IllegalAccessException See {@link Class#forName(java.lang.String)}
     * @throws InstantiationException See {@link Class#forName(java.lang.String)}
     * @throws DBBadPropertiesException Thrown if the properties provided to jLips are wrong
     * @throws SQLException Thrown if the database metadata could not be read
     * 
     * @see EntityFactory#init(Properties, Map)
     */
    public static void init(Map dbInitProp) 
        throws ClassNotFoundException,
        IllegalAccessException, 
        InstantiationException,
        DBBadPropertiesException,
        SQLException {
            
        String conHandlerClassName = (String)dbInitProp.get(EntityFactory.EF_DB_CON_HAND); 
        String propHandlerClassName = (String)dbInitProp.get(EntityFactory.EF_DB_PROP_HAND); 
        String sqlStatementHandlerClassName = (String)dbInitProp.get(EntityFactory.EF_DB_STM_HAND); 

        DBProperties connectionProperties = null;

	    if (propHandlerClassName == null) {
	        throw new DBBadPropertiesException("Bad handler - No DBProperties class supplied");
	    }
	    else {
		    /*
		     * Load the DBProperties class
		     */
		    // TODO Test the class implements DBProperties
		    try {
	            connectionProperties = (DBProperties)Class.forName(propHandlerClassName).newInstance();
	        } catch (ClassNotFoundException e) {
	            throw new ClassNotFoundException("Could not load the " + propHandlerClassName + " DBProperties class");
	        }
	    }

        initialiseObjects(conHandlerClassName, sqlStatementHandlerClassName);
	    
        // Pass the database properties to the connection properties
	    connectionProperties.setDBInitProperties(dbInitProp);

	    // Pass the connection properties to the connection handler
	    connectionHandler.setConnectionProperties(connectionProperties);
	    
        // Load all the database metadata 
        initialiseTableList();
	}
    
    /**
     * Initialises the database connectivity with provided DBProperties object.
     * 
     * This method will use the database connection properties provided in the
     * supplied DBProperties object instead of loading and instantiating a 
     * named connection properties object.
     * 
     * It will also use the default statement manager and connection manager
     *
     * @param connectionProperties Database connection properties object
     * @throws ClassNotFoundException Thrown if the classes specified in the database properties 
     * could not be loaded or instantiated
     * @throws IllegalAccessException See {@link Class#newInstance()}
     * @throws InstantiationException See {@link Class#newInstance()}
     * @throws DBBadPropertiesException Thrown if the properties provided to jLips are wrong
     * @throws SQLException Thrown if the database metadata could not be read
     * 
     * @see #DEFAULT_CON
     * @see #DEFAULT_STM
     */
    public static void init(DBProperties connectionProperties) 
	    throws ClassNotFoundException,
	    IllegalAccessException, 
	    InstantiationException,
	    DBBadPropertiesException,
	    SQLException {
	    
	    initialiseObjects(null, null);
	    
	    // Pass the database properties to the connection properties
	    connectionProperties.setDBInitProperties(null);
	
	    // Pass the connection properties to the connection handler
	    connectionHandler.setConnectionProperties(connectionProperties);
	    
	    // Load all the database metadata 
	    initialiseTableList();
    }

    /**
     * Request a connection from the associated connection handler
     * 
     * @return A {@link Connection Connection} object to the database
     * 
     * @see javax.sql.DataSource#getConnection()
     */
    public static Connection getConnection() throws SQLException {
        return connectionHandler.getConnection();
    }

    /**
     * Release a requested connection
     * 
     * After the application is done using the connection, it should
     * invoke this method to inform the connection handler that it doesn't 
     * need the connection anymore
     * 
     * @param theConnection The not needed connection
     */
    public static void releaseConnection(Connection theConnection) {
        connectionHandler.releaseConnection(theConnection);
    }

    /**
     * Get the DBProperties object
     * 
     * @return A reference to the database connection properties
     */
    public static DBProperties getDBProperties() {
        return connectionHandler.getConnectionProperties();
    }

    /**
     * Returns a List containing {@link DBTableInfo} objects for all the 
     * tables in the database
     * 
     * @return List containing DBTableInfo objects
     * @throws SQLException
     */
    public static List getTableNames() throws SQLException {
        return tableList;
    }

    /**
     * Returns a List containing {@link DBTableColumnInfo} objects for all the 
     * fields in the specified table
     * 
     * @param tableName The table info of the table
     * 
     * @return List containing DBTableColumnInfo objects
     * @throws SQLException
     */
    public static List getTableFields(DBTableInfo tableName) throws SQLException {
        return ((DBTableInfo)tableList.get(tableList.indexOf(tableName))).getColumnHandlerList();
    }

    /**
     * Get the last primary key field name for the specified table
     * 
     * @param tableName The table info of the table
     * 
     * @return A column handler for the primary key
     * @throws SQLException
     */
    public static DBTableColumnInfo getPrimaryKeyField(DBTableInfo tableName) throws SQLException {
        return ((DBTableInfo)tableList.get(tableList.indexOf(tableName))).getPrimaryKeyColumnHandler();
    }
    
    /**
     * Returns a new SQL statement from the statement manager
     *
     * @return Instance of new SQL statement
     * @throws InstantiationException See {@link Class#newInstance()}
     * @throws IllegalAccessException See {@link Class#newInstance()}
     */
    public static DBStatement createStatement() throws InstantiationException, IllegalAccessException {
        return (DBStatement)(sqlStatementHandler.newInstance());
    }
    
    /**
     * Initialises the connection handler and the statement handler
     * 
     * If the provided handlers are null, then the default ones will be used
     *
     * @param conHandlerClassName
     * @param sqlStatementHandlerClassName
     * @throws ClassNotFoundException Thrown if the classes specified in the database properties 
     * could not be loaded or instantiated
     * @throws InstantiationException See {@link Class#newInstance()}
     * @throws IllegalAccessException See {@link Class#newInstance()}
     * @throws SQLException Thrown if the database metadata could not be read
     * 
     * @see #DEFAULT_CON
     * @see #DEFAULT_STM
     */
    private static void initialiseObjects(String conHandlerClassName, String sqlStatementHandlerClassName) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
	    /*
	     * If no connection handler is supplied, the default one will be used
	     */
	    if (conHandlerClassName == null) {
	        conHandlerClassName = DEFAULT_CON;
	    }
	
	    /*
	     * If no statement handler is supplied, the default one will be used
	     */
	    if (sqlStatementHandlerClassName == null) {
	        sqlStatementHandlerClassName = DEFAULT_STM;
	    }
	    
	    /*
	     * Load the SQL statement handler
	     */
        try {	
            sqlStatementHandler = Class.forName(sqlStatementHandlerClassName);
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("Could not load the " + sqlStatementHandlerClassName + " DBStatement class");
        }

	
	    try {
            connectionHandler = (DBConnectionHandler) Class.forName(conHandlerClassName).newInstance();
	    } catch (ClassNotFoundException e1) {
            throw new ClassNotFoundException("Could not load the " + conHandlerClassName + " DBConnectionHandler class");
        }
    }
    
    /**
     * Reads the database meta data
     * 
     * The metadata is stored in the connection handler and made available for
     * jLips by the methods:
     * <ul>
     * <li>{@link #getTableFields(DBTableInfo)} - Returns a List of the tables</li>
     * <li>{@link #getPrimaryKeyField(DBTableInfo)} - Returns the primary key column</li>
     * <li>{@link #getTableFields(DBTableInfo)} - Returns the table columns</li>
     * </ul>
     * 
     * All table information are returned as {@link DBTableInfo} objects and all
     * column information are returned as {@link DBTableColumnInfo} objects
     *
     * @throws SQLException Thrown if the database metadata could not be read
     */
    private static void initialiseTableList() throws SQLException {
        Connection myCon = DBConnectionFactory.getConnection();

        /* Get the database meta-data object */
        DatabaseMetaData dbmd = myCon.getMetaData();
        
        tableList = new Vector();

        try {
            String schema = getDBProperties().getSchema();
            ResultSet rs = dbmd.getTables(null, schema, "%", null);

            while (rs.next()) {
                DBTableInfo ti = new DBTableInfo(rs.getString("TABLE_NAME"));

                tableList.add(ti);
            }
        } catch (Exception e) {
            e.printStackTrace(); // EXCEPTION
        }
        
        for (Iterator iter = tableList.iterator(); iter.hasNext();) {
            DBTableInfo element = (DBTableInfo) iter.next();

            String 	primaryKeyName 		= null;

            try {
	            ResultSet rs = dbmd.getPrimaryKeys(null, getDBProperties().getSchema(), element.getTableName());
	
	            /*
	             * If there are more than one primary key, this method will only return
	             * the last in the list.
	             * 
	             * As the order of the primary keys are undefined, it might produce
	             * undefined results if there are more than one primary key
	             */
	            while (rs.next()) {
	                primaryKeyName = rs.getString("COLUMN_NAME");
	            }
	        } catch (Exception e) {
	            e.printStackTrace(); // EXCEPTION
	        }
		        
            List 	columnNameList 		= new Vector();
            int 	primaryKeyIndex 	= 0;

            try {
	            ResultSet rs = dbmd.getColumns(null, getDBProperties().getSchema(), element.getTableName(), "%");
	
	            while (rs.next()) {
	                DBTableColumnInfo ci = new DBTableColumnInfo(rs.getString("COLUMN_NAME"));
	                
	                // Additional column info
	                ci.theTableInfo = element;
	                ci.columnSize = rs.getInt("COLUMN_SIZE");
	                ci.sqlType = rs.getShort("DATA_TYPE");
	
	                columnNameList.add(ci);
	                
	                if (ci.getColumnName().equals(primaryKeyName))
	                    primaryKeyIndex = columnNameList.size() - 1;
	            }
	        } catch (Exception e) {
	            e.printStackTrace(); // EXCEPTION
	        }
	        
	        element.setColumnInfo(columnNameList, primaryKeyIndex);
        }

        releaseConnection(myCon);
    }
}