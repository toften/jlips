/*
 * This is the script used to create the menu on the individual pages
 *
 * It will show the current page as highlighted.
 * It requires CSS to provide the formatting.
 *
 *
 * The CSS classes used are:
 * Table: menu
 *
 * $Log: menu.js,v $
 *
 */

var logos = [
                [
                    "http://sourceforge.net/",
                    "jLips Sourceforge summary page",
                    "http://sourceforge.net/sflogo.php?group_id=68578&amp;type=1"
                ],
                [
                    "http://www.spreadfirefox.com/?q=affiliates&amp;id=25858&amp;t=70",
                    "Get Firefox!",
                    "http://www.spreadfirefox.com/community/images/affiliates/Buttons/88x31/get.gif"
                ],
                [
                    "http://toften.blogspot.com/",
                    "Toftens blog",
                    "http://buttons.blogger.com/bloggerbutton1.gif"
                ],
                [
                    "http://validator.w3.org/check/referer",
                    "Valid XHTML 1.0!",
                    "http://www.w3.org/Icons/valid-xhtml10"
                ]
            ]
 
var main_menu_items = [   ["index.html", 				    								 "Home", 									0], 
                ["use.html", 												 "Use", 									0],
                ["use/getting_started.html",    					 "Getting started", 	    1],
                ["use/tutorial.html", 										 "Tutorials", 						1],
                ["use/docs.html", 									 "Documentation", 				1],
                ["use/docs/naming.html", 									 "Naming rules", 				2],
//                ["use/modelling.html", 		 							   "UML mapping", 		 		  1],
                ["use/api/index.html", 										 "User API", 						  2],
                ["dev.html", 			    							 "Development", 		    	0],
//                ["dev/arch.html",  			 								   "Architecture", 		 		  1],
//                ["dev/conventions.html", 		 							 "Code conventions", 	 		1],
//                ["dev/glossary.html", 		 								 "Glossary", 			 				1],
//                ["dev/api/index.html", 		 								 "Developer API", 		 		1],
                ["download.html", 												 "Download", 							0],
                ["version_history.html", 									 "Version history", 	    0],
								["faq.html", 															 "FAQ", 									0],
								["http://sourceforge.net/projects/jlips/", "SF Summary Page", 									0]
            ]
