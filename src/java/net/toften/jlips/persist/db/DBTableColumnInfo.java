/*
 * (C) 2003 toften.net
 *
 * DBTableColumnInfo.java
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBTableColumnInfo.java,v $
 * Revision 1.4  2004/12/07 10:49:35  toften
 * Now stores the data internally instead of accessing the database
 *
 * Revision 1.3  2004/11/07 16:15:15  toften
 * Moved to different more relevant package
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2003/11/02 17:49:14  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

import java.sql.Types;

/**
 * DOCUMENT ME!
 *
 * @author thomas
 * @version $Revision: 1.4 $
 */
public class DBTableColumnInfo {
    
    DBTableInfo theTableInfo;
    
    /** DOCUMENT ME! */
    private String columnName;

    /** DOCUMENT ME! */
    int columnSize;

    /** DOCUMENT ME! */
    short sqlType;

    /**
     * DOCUMENT ME!
     *
     * @param columnName
     */
    public DBTableColumnInfo(String columnName) {
        this.columnName = columnName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return Returns the columnName.
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return Returns the columnSize.
     */
    public int getColumnSize() {
        return columnSize;
    }

    /**
     * DOCUMENT ME!
     *
     * @return Returns the sqlType.
     */
    public short getSqlType() {
        return sqlType;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return getColumnName();
    }

	/**
	 * This method will adjust the quotes around a value.  The quotes will
	 * be applied if the type of the field is:   For any other type the
	 * value will remain unchanged.
	 *
	 * @param value The object containing the value. The object passed in
	 *        will be converted into a string using the {@link
	 *        java.lang.Object#toString()} method
	 *
	 * @return The value as a string either with or without quotes,
	 *         depending of the type
	 */
	public String adjustQuotes(Object value) {
	    String returnStr;
	
	    // TODO Deal with other types
	    switch (getSqlType()) {
	        case Types.CHAR:
	        case Types.VARCHAR:
	            returnStr = "'" + value.toString() + "'";
	
	            break;
	
	        default:
	            returnStr = value.toString();
	
	            break;
	    }
	
	    return returnStr;
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @param value DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public String getEqualString(Object value) {
	    return getColumnName() + " = " + adjustQuotes(value);
	}
}
