/*
 * jLips
 *
 * Copyright (C) 2004 toften.net
 * 
 * DBJNDIProperties.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBJNDIProperties.java,v $
 * Revision 1.2  2004/11/17 19:49:40  toften
 * Updated documentation
 *
 * Revision 1.1  2004/10/30 12:24:20  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist.db;

import java.util.Map;
import java.util.Properties;

/**
 * This is the database connection properties for using a JNDI connection
 * 
 * The application must provide the JNDI URI to jLips. This is done when jLips
 * is initialised.
 * 
 * <pre>
 * Properties dbProbs = new Properties();
 * dbProbs.put(EntityFactory.EF_DB_CON_HAND, "net.toften.jlips.persist.db.DBJNDIConnection");
 * dbProbs.put(EntityFactory.EF_DB_PROP_HAND, "net.toften.jlips.persist.db.DBJNDIProperties");
 * dbProbs.put(DBJNDIProperties.DB_JNDI_URI, "java:comp/env/jdbc/jlipsDB");
 * </pre>
 * In the above example the database properties has one extra entry: The JNDI URI.
 * The key for the JNDI URI property is defined in {@link #DB_JNDI_URI here}
 * 
 * @author thomas
 * 
 * @see net.toften.jlips.persist.db.DBJNDIConnection
 * @see net.toften.jlips.persist.EntityFactory#init(Properties, Map)
 */
public class DBJNDIProperties extends DBAbstractProperties implements DBProperties {
    /**
     * String used to specify the JNDI URI in the database properties passed
     * by the application to jLips
     */
    public static final String DB_JNDI_URI = "connection.jndi.uri";
     
    /**
     * Placeholder for the JNDI URI
     */
    private String jini_uri;

    
    /**
     * This is overridden to return the JNDI URI instead of the database
     * URL
     * 
     * @return JNDI URI
     * 
     * @see net.toften.jlips.persist.db.DBAbstractProperties#getURL()
     */
    public String getURL() {
        return jini_uri;
    }
    
    /**
     * @see DBProperties#setDBInitProperties(Map)
     */
    public void setDBInitProperties(Map dbInitProbs) {
    	// Copy the JNDI property value into the URL property value of the database properties
        jini_uri = (String)dbInitProbs.get(DB_JNDI_URI);
    }
}
