/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * DBWrappedConnection.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBWrappedConnection.java,v $
 * Revision 1.2  2004/11/17 19:55:21  toften
 * Removed implemented interface - should be specified in the concrete class
 *
 * Revision 1.1  2004/11/08 23:02:54  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * This class wraps an instance of a {@link javax.sql.DataSource DataSource}
 * 
 * The class is abstract, so the extending class must set up the wrapped DataSource
 * 
 * @author thomas
 *
 */
public abstract class DBWrappedConnection extends DBAbstractConnection {
    protected DataSource wrappedDS = null;

    /**
     * @see javax.sql.DataSource#getConnection()
     */
    public Connection getConnection() throws SQLException {
        return wrappedDS.getConnection();
    }
    /**
     * @see javax.sql.DataSource#getConnection(java.lang.String, java.lang.String)
     */
    public Connection getConnection(String arg0, String arg1)
            throws SQLException {
        return wrappedDS.getConnection(arg0, arg1);
    }
    /**
     * @see javax.sql.DataSource#getLoginTimeout()
     */
    public int getLoginTimeout() throws SQLException {
        return wrappedDS.getLoginTimeout();
    }
    /**
     * @see javax.sql.DataSource#getLogWriter()
     */
    public PrintWriter getLogWriter() throws SQLException {
        return wrappedDS.getLogWriter();
    }
    /**
     * @see javax.sql.DataSource#setLoginTimeout(int)
     */
    public void setLoginTimeout(int arg0) throws SQLException {
        wrappedDS.setLoginTimeout(arg0);
    }
    /**
     * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
     */
    public void setLogWriter(PrintWriter arg0) throws SQLException {
        wrappedDS.setLogWriter(arg0);
    }
}
