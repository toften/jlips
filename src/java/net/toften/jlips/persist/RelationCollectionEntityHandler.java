/*
 * (C) 2003 toften.net
 *
 * RelationCollectionEntityHandler.java
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: RelationCollectionEntityHandler.java,v $
 * Revision 1.4  2004/11/17 19:02:43  toften
 * Updated documentation
 * Enabled the use of multiple persiste entities to one database table
 * Removed un-implemented method. They will now be caught by the AbstractCollectionEntityHandler and an exception will be thrown
 *
 * Revision 1.3  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.2  2003/01/07 23:24:35  toften
 * Updated various comments.
 * Added more support for Collections
 *
 * Revision 1.1  2002/12/16 14:48:30  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist;

import java.util.Collection;
import java.util.List;


/**
 * This class holds a collection of entities that is part of a
 * one-to-many relation between two tables.
 * 
 * The methods in this class allows the user to manipulate this
 * relationship by adding and removing entities. By adding or removing
 * entities, we create and destroy relationships between records
 * in the database.
 *
 * @author thomas
 * @version $Revision: 1.4 $
 */
class RelationCollectionEntityHandler extends AbstractCollectionEntityHandler
    implements Collection {
    
    /**
     * Place holder for the table handler for the main table
     */
    private TableHandler mainTableHandler;
    
    /**
     * Primary key for the parrent record
     */
    private Object pKey;
    
    /**
     * Constructor for RelationCollectionEntityHandler
     * 
     * Creates a new RelationCollectionEntityHandler object.
     * 
     * @param relatedTableHandler Table handler for the related table
     * @param values List containing the primary keys of the related records 
     * @param mainTableHandler Table handler for the main (parrent) table
     * @param pKey Primary key for the main table
     */
    RelationCollectionEntityHandler(TableHandler relatedTableHandler, Class relatedEntity, List values,
            TableHandler mainTableHandler, Object pKey) {
        super(relatedTableHandler, relatedEntity);

        this.records 			= values;
        this.mainTableHandler 	= mainTableHandler;
        this.pKey 				= pKey;
    }

    /**
     * This method will add a relation to the one-to-many
     * relationship defined by this object.
     * 
     * @param o The entity that should be added to the 
     * relationship
     * 
     * @see java.util.Collection#add(java.lang.Object)
     */
    public boolean add(Object o) {
        boolean returnValue = false;
        
        // Treat o as a PersistEntity type
        PersistEntity persistObject = (PersistEntity)o;
        
        /* 
         * First check if the entity is of the right type.
         * It must be of the same type as all other entities in
         * this collection
         */
        // TODO this is wrong! The relatedEntity should be compared with o's interfaces
        if (persistObject.getTableName().equals(collectedTableHandler.getTableName())) {
            String relationFieldname = mainTableHandler.getTableName() + mainTableHandler.getPrimaryKeyName();
            
            persistObject.set(relationFieldname, pKey);
            
            records.add(o);
            returnValue = true;
        }
            
        return returnValue;
    }
}
