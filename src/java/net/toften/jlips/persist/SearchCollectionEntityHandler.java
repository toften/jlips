/*
 * (C) 2003 toften.net
 *
 * SearchCollectionEntityHandler.java
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: SearchCollectionEntityHandler.java,v $
 * Revision 1.4  2004/11/17 19:07:18  toften
 * Updated documentation
 * Enabled the use of multiple persiste entities to one database table
 *
 * Revision 1.3  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.2  2003/01/07 23:24:35  toften
 * Updated various comments.
 * Added more support for Collections
 *
 * Revision 1.1  2002/12/16 14:48:20  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist;

import java.util.List;
import java.util.Collection;


/**
 * This class holds a collection of records that has been searched 
 * from the database
 * 
 * The main difference between this collection and the {@link net.toften.jlips.persist.RelationCollectionEntityHandler}
 * class is that this class will not allow any changes to the collection itself.
 * It is then not allowed to add, remove, etc. anything from this collection
 *
 * @author thomas
 * @version $Revision: 1.4 $
 */
class SearchCollectionEntityHandler extends AbstractCollectionEntityHandler
    implements Collection {
    /**
     * Creates a new SearchCollectionEntityHandler object.
     *
     * @param searchedEntity The persist entity of the found records
     * @param searchedTableHandler The table handler for the found records
     * @param searchedRecords The primary keys of the found records
     */
    SearchCollectionEntityHandler(TableHandler searchedTableHandler, Class searchedEntity,
        List searchedRecords) {
        super(searchedTableHandler, searchedEntity);

        this.records = searchedRecords;
    }
}
