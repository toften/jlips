/*
 * jlips
 *
 * Copyright (C) 2005 toften.net
 * 
 * DatabaseHandler.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: ThomasLarsen
 *
 * File change log:
 * ----------------
 * $Log: DatabaseHandler.java,v $
 * Revision 1.1  2005/02/13 17:18:19  toften
 * Added DatabaseHandler layer
 *
 */

package net.toften.jlips.persist;

import java.util.Collection;

/**
 * DOCUMENT ME!
 *
 * @author ThomasLarsen
 *
 */
interface DatabaseHandler {

    /**
     * This method is used to find a specific record in the table. The find
     * method will only return one record.
     *
     * @param key The primary key of the record. This is of the type {@link
     *        Object} so that any type of primary key can be used
     *
     * @return Object representing the record. Must be typecasted to the
     *         interface that is used to access the table
     */
    Object find(Class theEntity, Object key);

    /**
     * Returns a collection of all the records in the table
     *
     * @return Collection with all records
     */
    Collection findAll(Class searchEntity);

    /**
     * This method is used to create a new record in a table.
     *
     * When the application needs to create a new record in the
     * database, the {@link EntityFactory#create(Class) create}
     * method is invoked. This method in turn invokes this method
     * which is responsible for creating the actual record in the
     * database.
     * After the record has been created, a {@link SingleEntityHandler}
     * object is created, which represents the record to the
     * application.
     *
     * <b>Primary key</b>
     * When the record is created a primary key is generated
     * automatically using the {@link #getNextPrimaryKey())
     * method
     *
     * @return reference to the new object
     */
    Object create(Class theEntity);
    
    /**
     * DOCUMENT ME!
     *
     * @param key
     */
    void remove(Object key);

    /**
     * DOCUMENT ME!
     *
     * @param searchName DOCUMENT ME!
     * @param args DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    Collection getSearchCollection(Class searchEntity, String searchName, Object[] args);
}
