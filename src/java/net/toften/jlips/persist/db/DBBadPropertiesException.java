/*
 * (C) 2003 toften.net
 *
 * DBBadPropertiesException.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBBadPropertiesException.java,v $
 * Revision 1.4  2004/11/17 23:23:25  toften
 * Updated documentation
 *
 * Revision 1.3  2004/11/08 23:04:11  toften
 * Removed unnecessary methods
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2002/12/16 14:48:46  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist.db;

/**
 * This exception is thrown if the databse properties provided
 * by the application are either missing or wrong
 *
 * @author thomas
 * @version $Revision: 1.4 $
 */
public class DBBadPropertiesException extends Exception {
    /**
     * Creates a new DBBadPropertiesException object.
     *
     * @param cause The description of the cause of the exception
     */
    DBBadPropertiesException(String cause) {
        super(cause);
    }
}
