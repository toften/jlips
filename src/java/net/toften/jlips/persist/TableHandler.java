/*
 * (C) 2003 toften.net
 *
 * TableHandler.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: TableHandler.java,v $
 * Revision 1.9  2005/02/13 17:17:49  toften
 * Added DatabaseHandler layer
 *
 * Revision 1.8  2004/12/07 10:51:38  toften
 * Persist interfaces are now mapped to tables
 *
 * Revision 1.7  2004/11/17 23:45:00  toften
 * Updated documentation
 * Enabled the use of multiple persiste entities to one database table
 *
 * Revision 1.6  2004/11/08 23:00:52  toften
 * Spun out the primary key handling to the PrimaryKeyHandler group of classes
 * Cleaned up the use of the qoute adjust methods
 * Removed the direct primarykey get/set methods - should now be called through the primary key handler
 *
 * Revision 1.5  2004/11/07 16:15:15  toften
 * Moved to different more relevant package
 *
 * Revision 1.4  2004/10/30 12:24:19  toften
 * Initial checkin
 *
 * Revision 1.3  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.2  2003/01/07 23:24:35  toften
 * Updated various comments.
 * Added more support for Collections
 *
 * Revision 1.1  2002/12/16 14:48:20  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import net.toften.jlips.persist.db.DBConnectionFactory;
import net.toften.jlips.persist.db.DBStatement;
import net.toften.jlips.persist.db.DBTableColumnInfo;
import net.toften.jlips.persist.db.DBTableInfo;
import net.toften.jlips.persist.db.pk.PrimaryKeyHandler;


/**
 * Handles the format and the communication with a table in the database  This
 * class holdes information about <b>one</b> table only, which means there mut
 * be one TableHandler class for each table that is to be used from the
 * database.  The TableHandler object is created by the {@link
 * TableHandlerFactory#init(HashMap)} method which must be invoked when the
 * application is started up
 *
 * @author toften
 * @version $Revision: 1.9 $
 */
final class TableHandler {
    /**
     * Holder for the field names of the table  When a {@link ColumnHandler} is
     * created it is added to this map. the <i>key</i> will be the name of the
     * column and the <i>value</i> will be the column handler object.
     * 
     * The key (i.e. the column name) will be stored in lower case
     *
     * @see ColumnHandler#ColumnHandler(DBTableColumnInfo)
     */
    private HashMap fieldsColumnHandlers = new HashMap();

    /**
     * Holder for the info of the table  To get the name of the table as a
     * String, the  {@link DBTableInfo#getTableName() getTableName} method
     * should be used
     */
    private final DBTableInfo tableInfo;

    private final PrimaryKeyHandler primaryKeyHandler;

    /**
     * Creates a new TableHandler object. This object should <b>only</b> be
     * created from the {@link TableHandlerFactory} class.
     *
     * @param tableInfo the tabel in the database
     */
    TableHandler(DBTableInfo tableInfo, PrimaryKeyHandler PKH) {
        this.tableInfo = tableInfo;
        this.primaryKeyHandler = PKH;        
    }

    /**
     * Returns the name of the table as it is in the database. This will be the
     * same as the name of the interface used to  communicate with the table
     *
     * @return String containing the table name
     *
     * @see EntityFactory#getEntityName(Class)}
     */
    final String getTableName() {
        return tableInfo.getTableName();
    }

    /**
     * This method is used to find a specific record in the table. The find
     * method will only return one record.
     *
     * @param key The primary key of the record. This is of the type {@link
     *        Object} so that any type of primary key can be used
     *
     * @return Object representing the record. Must be typecasted to the
     *         interface that is used to access the table
     */
    Object find(Class theEntity, Object key) {
        Object returnObj = null;
        DBStatement dbs = null;
        
        if (key != null) {
	        try {
				dbs = DBConnectionFactory.createStatement();
				dbs.addSourceTable(tableInfo);
				dbs.setPrimaryKeyValue(key);
		        ResultSet rs = dbs.doSelect();
	
	            /* Get the values */
	            HashMap values = new HashMap();
	        	
	            while (rs.next()) {
	                ResultSetMetaData rsmd = rs.getMetaData();
	                int i                  = 1;
	
	                while (i <= rsmd.getColumnCount()) {
	                    values.put(rsmd.getColumnName(i), rs.getObject(i));
	                    i++;
	                }
	
	                break;
	            }
	
	            returnObj = SingleEntityHandler.create(theEntity, values, key, this);
	        } catch (Exception e) {
	            e.printStackTrace(); // EXCEPTION
	        } finally {
	        	dbs.release();        	
	        }
        }

        return returnObj;
    }

    /**
     * Returns a collection of all the records in the table
     *
     * @return Collection with all records
     */
    Collection findAll(Class searchEntity) {
        Collection returnObj = null;
        DBStatement dbs = null;

        try {
    		dbs = DBConnectionFactory.createStatement();
    		dbs.addSourceTable(tableInfo);
            dbs.addReturnColumn(getPrimaryKeyHandler().getColumnInfo());

			Vector values = new Vector();
			ResultSet rs = dbs.doSelect();

            /* Get the values */
            while (rs.next()) {
                values.add(rs.getObject(getPrimaryKeyName()));
            }

            returnObj = new SearchCollectionEntityHandler(this, searchEntity, values);
        } catch (Exception e) {
            e.printStackTrace(); // EXCEPTION
        } finally {
            dbs.release();
        }

        // System.out.println(stmt);
        return returnObj;
    }

    /**
     * DOCUMENT ME!
     *
     * @param key DOCUMENT ME!
     * @param values DOCUMENT ME!
     */
    void update(Object key, Map values) {
        if (key != null) { // Don't do anything with no primary key
            Object[] keys       = values.keySet().toArray();
            /* Don't do anyting if no new values
             * This should be checked by the calling method!!
             */
            if (keys.length > 0) {
	            try {
					DBStatement dbs = DBConnectionFactory.createStatement();
					dbs.addSourceTable(tableInfo);
					dbs.setPrimaryKeyValue(key);

					for (int i = 0; i < keys.length; i++) {
	                    String columnName = (String)keys[i];
	                    dbs.addEqualExpression(getColumnHandler(columnName).columnInfo, values.get(columnName));
	                }
					
					dbs.doUpdate();
                } catch (Exception e) {
                    e.printStackTrace(); // EXCEPTION
                }
            }
        }
    }

    /**
     * This method is used to create a new record in a table.
     *
     * When the application needs to create a new record in the
     * database, the {@link EntityFactory#create(Class) create}
     * method is invoked. This method in turn invokes this method
     * which is responsible for creating the actual record in the
     * database.
     * After the record has been created, a {@link SingleEntityHandler}
     * object is created, which represents the record to the
     * application.
     *
     * <b>Primary key</b>
     * When the record is created a primary key is generated
     * automatically using the {@link #getNextPrimaryKey())
     * method
     *
     * @return reference to the new object
     */
    Object create(Class theEntity) {
        Object returnObj = null;

        // Create the record in the database
        try {
            Object key   = getPrimaryKeyHandler().getNextPrimaryKey();

            DBStatement dbs = DBConnectionFactory.createStatement();
			dbs.addSourceTable(tableInfo);
			dbs.setPrimaryKeyValue(key);
			
			dbs.doInsert();

            /* Set the values */
            HashMap values = new HashMap();

            values.put(getPrimaryKeyName(), key);

            returnObj = SingleEntityHandler.create(theEntity, values, key, this);
        } catch (Exception e) {
            e.printStackTrace(); //EXCEPTION
        }

        // System.out.println(stmt);
        return returnObj;
    }

    /**
     * DOCUMENT ME!
     *
     * @param key
     */
    void remove(Object key) {
        if (key != null) {
            try {
                DBStatement dbs = DBConnectionFactory.createStatement();
    			dbs.addSourceTable(tableInfo);
    			dbs.setPrimaryKeyValue(key);
    			
    			dbs.doDelete();
            } catch (Exception e) {
                e.printStackTrace(); //EXCEPTION
            }            
        }
    }
    
    /**
     * DOCUMENT ME!
     *
     * @param searchName DOCUMENT ME!
     * @param args DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    Collection getSearchCollection(Class searchEntity, String searchName, Object[] args) {
        Collection returnObj = null;
        String query         = null;
        String[][] queries   = null;

        String fId = getPrimaryKeyName();

        try {
            queries = (String[][])searchEntity.getField("queries").get(null);
        } catch (Exception e) {
        	// EXCEPTION
        }

        /* Find the search query */
        for (int i = 0; i < queries.length; i++) {
            if (queries[i][0].equals(searchName)) {
                query = queries[i][1];

                break;
            }
        }

        /*
         * Find and fill in the '?'
         */
        if (query != null) {
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    int qm = query.indexOf('?');

                    if (qm > 0) {
                        query = query.substring(0, qm) + adjustQuotes(args[i]) +
                            query.substring(qm + 1);
                    } else {
                        break;
                    }
                }
            }

            // TODO use DBStatement 
            query = "select " + fId + " from " + tableInfo + " where " + query;

            // System.out.println(query);
            Connection myCon = null;

            try {
                Vector values = new Vector();

                myCon = DBConnectionFactory.getConnection();

                Statement s = myCon.createStatement();

                ResultSet rs = s.executeQuery(query);

                /* Get the values */
                while (rs.next()) {
                    values.add(rs.getObject(getPrimaryKeyName()));
                }

                returnObj = new SearchCollectionEntityHandler(this, searchEntity, values);
            } catch (Exception e) {
                e.printStackTrace(); // EXCEPTION
            } finally {
                DBConnectionFactory.releaseConnection(myCon);
            }
        }

        return returnObj;
    }

    /**
     * Creating a collection that contains a list of records defined
     * in a one-to-many relationship.
     * 
     * Is used from the point of view of the <i>one</i> record.
     * 
     * In the persist interface the code will look like this:
     * <pre>
     * public interface Person {
     * 	...
     * 	Collection getAddress();
     * 	...
     * }
     * </pre>
     * 
     * The related persist interface will then be:
     * <pre>
     * public interface Address {
     * 	...
     * 	Person getPerson();
     * }
     * </pre>
     * 
     * Assuming that the <code>Person</code> interface is mapped to a
     * <i>person</i> table and the <code>Address</code> interface is
     * mapped to a <i>address</i> table, the <code>relatedEntity</code>
     * will then be the <code>Address</code> interface and the
     * table handler this method will be invoked on it the table handler
     * of the <code>Person</code> interface.
     * 
     * In the <i>address</i> table jLips will expect to find a field
     * with the name <code>personid</code> (assuming the primary key
     * of the <i>person</i> table is called <code>id</code>).
     *
     * @param relatedEntity The name of the related table, i.e. the
     * <i>many</i> table
     * @param pKey The primary key of the <i>one</i> record
     *
     * @return A Collection object containing the related records.
     * If there are no related records the Collection will be
     * empty, but still returned
     */
    Collection getCollectionRelation(Class relatedEntity, Object pKey) {
        Collection returnObj  = null;
        DBStatement dbs = null;

        /* We don't know the TableHandler for the related records
         * We get it from the TableHandlerFactory
         */
        TableHandler relatedTableHandler 	= (TableHandler)TableHandlerFactory.getHandler(relatedEntity);
        String foreginKeyName 				= getTableName() + getPrimaryKeyName(); // field in the related table

        try {
            // Build the expression to return the related records
    		dbs = DBConnectionFactory.createStatement();
    		dbs.addSourceTable(relatedTableHandler.tableInfo);
    		dbs.addReturnColumn(getPrimaryKeyHandler().getColumnInfo()); // return primary keys for our table
    		dbs.addEqualExpression(relatedTableHandler.getColumnHandler(foreginKeyName).columnInfo, pKey);

			ResultSet rs = dbs.doSelect();

            /* Get the values */
			Vector values = new Vector();
			while (rs.next()) {
                values.add(rs.getObject(getPrimaryKeyName()));
            }

            returnObj = new RelationCollectionEntityHandler(relatedTableHandler, relatedEntity, values, this, pKey);
        } catch (Exception e) {
            System.out.println(e.toString()); // EXCEPTION
        } finally {
        	dbs.release();
        }

        // System.out.println(stmt);
        return returnObj;
    }

    /**
     * DOCUMENT ME!
     *
     * @param theEntity The Persist entity that must be returned
     * @param pKey The primary key of the 
     *
     * @return DOCUMENT ME!
     */
    Object getSingleRelation(Class targetEntity, Object pKey) {
        Object returnObj      = null;

        TableHandler targetTh = (TableHandler)TableHandlerFactory.getHandler(targetEntity);

        returnObj             = targetTh.find(targetEntity, pKey);

        return returnObj;
    }

    /**
     * DOCUMENT ME!
     *
     * @return
     */
    private PrimaryKeyHandler getPrimaryKeyHandler() {
        return primaryKeyHandler;
    }
    
    /**
     * Will return a column handler for a named column. The name
     *
     * @param columnName The name of the column
     *
     * @return the handler for the requested column
     */
    final ColumnHandler getColumnHandler(String columnName) {
        ColumnHandler returnCh = (ColumnHandler)fieldsColumnHandlers.get(columnName);

        return returnCh;
    }

    /**
     * DOCUMENT ME!
     *
     * @return The name of the primary key field
     */
    final String getPrimaryKeyName() {
        return getPrimaryKeyHandler().getColumnInfo().getColumnName();
    }

    /**
     * DOCUMENT ME!
     *
     * @param value DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     * @see #getSearchCollection(Class, String, Object[])
     */
    protected static String adjustQuotes(Object value) {
        String returnStr;

        if (value instanceof String) {
            returnStr = "'" + value + "'";
        } else {
            returnStr = value.toString();
        }

        return returnStr;
    }

    /**
     * DOCUMENT ME!
     *
     * @author thomas
     * @version $Revision: 1.9 $
     */
    class ColumnHandler {
        /* SQL related fields */

        /**
         * Holder for the name of the column. This will be the same name as the
         * field in the database
         */
        private final DBTableColumnInfo columnInfo;

        /**
         * Creates a new ColumnHandler object.
         *
         * @param info The name of the column
         */
        ColumnHandler(DBTableColumnInfo info) {
            this.columnInfo = info;

            /* Add the column to the TableHandler's list of fields */
            fieldsColumnHandlers.put(columnInfo.getColumnName().toLowerCase(), this);
        }
    }
}
