/*
 * (C) 2003 toften.net
 *
 * PersistEntity.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: PersistEntity.java,v $
 * Revision 1.4  2004/11/17 19:15:55  toften
 * Updated documentation
 *
 * Revision 1.3  2004/11/08 22:58:35  toften
 * Added remove method
 *
 * Revision 1.2  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.1  2002/12/16 14:48:22  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist;

/**
 * The base interface that all the interfaces specifying the accessor methods
 * should extend
 * 
 * This interface provides common methods for all the persist interfaces
 * as well as a number of database related methods.
 *
 * @author thomas
 * @version $Revision: 1.4 $
 */
public interface PersistEntity {
    /**
     * Returns the primary key
     * 
     * Note: This method is package scoped, as it should only be used internally
     * in jLips.
     * If the application needs access to the primary key, it should provide a
     * get method for the primary key field 
     *
     * @return Object containing the primary key
     */
    Object getPrimaryKey();

    /**
     * Commits all changes made to the object to the database.  When accessor
     * methods are invoked the values supplied are not actually written to the
     * database; they are just held in a temporary store. This is done to
     * minimize the communication to the database.  This method forces the
     * temporary data to be written to the database
     */
    public void commit();

    /**
     * The changes made to the data since the last commit will be discarted
     */
    public void rollback();

    /**
     * Removes the record from the database
     *
     * Note, that after this method has been called, the reference to the entity
     * should be discarted. Any attempts to invoke methods on the entity will
     * result in an exception being thrown
     * 
     * @see EntityNotFoundException
     */
    public void remove();
    
    /**
     * Generic get method
     * 
     * This method will return the value any named field in the record.
     *
     * @param fieldName Fieldname to be returned
     *
     * @return The value of the field
     */
    public Object get(String fieldName);

    /**
     * Generic set method
     * 
     * This method will set the value of any named field in the record to
     * the specified value
     *
     * @param fieldName Fieldname to be modified
     * @param value New field value
     */
    public void set(String fieldName, Object value);
    
    /**
     * Returns the name of the table this entity is related to
     * 
     * @return Table name of related table
     */
    public String getTableName();
}
