/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * DBStatement.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBStatement.java,v $
 * Revision 1.2  2004/12/07 10:49:02  toften
 * Added methods for update, insert and delete
 *
 * Revision 1.1  2004/11/07 16:15:15  toften
 * Moved to different more relevant package
 *
 * Revision 1.1  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 *
 *
*/

package net.toften.jlips.persist.db;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This interface defines the methods for building a SQL statement.
 * 
 * The interface provides a set of methods used to build up a SQL statement.
 * The mehods use other jLips database description objects as parameters.
 * 
 * The different parts of the statement are first set up:
 * <ul>
 * <li>Return columns</li>
 * <li>Search expresions</li>
 * <li>Source tables</li>
 * </ul>
 * 
 * After the statement parts are set up, the type if request is chosen:
 * <ul>
 * <li>Select statement</li>
 * <li>Update statement</li>
 * <li>Insert statement</li>
 * <li>Delete statement</li>
 * </ul>
 * 
 * <h2>Implmenting the interface</h2>
 * The interface can be implemented by the application. When the application
 * needs a connection the the database, the {@link net.toften.jlips.persist.db.DBConnectionFactory#getConnection()}
 * method is used.
 * 
 * <h2>Selecting the implmentation jLips uses</h2>
 * When jLips is initialised using the {@link net.toften.jlips.persist.EntityFactory#init(Properties, Map)}
 * method, passing in the name of the DBStatement implementing class.
 * The class is passed in the database properties parameter using the {@link net.toften.jlips.persist.EntityFactory#EF_DB_STM_HAND}
 * 
 * <h2>Implementations in jLips</h2>
 * jLips provides a default implementation as well as an abstract class, that the application
 * can extend for it's own implementation of the SQL statement
 *
 * @author thomas
 *
 * @see net.toften.jlips.persist.db.DBDefaultStatement
 */
public interface DBStatement {
    /**
     * Add a table to the statement
     * 
     * This applies to:
	 * <ul>
	 * <li>Select statement (data is returned from the table)</li>
	 * <li>Update statement (data is updated in the table)</li>
	 * <li>Insert statement (a record is inserted in the table)</li>
	 * <li>Delete statement (data is removed from the table)</li>
	 * </ul>
     *
     * @param newTable The table to add 
     */
    public void addSourceTable(DBTableInfo newTable);

    /**
     * Adds an equal expresion to the statement
     * 
     * This applies to:
	 * <ul>
	 * <li>Select statement (the values to be searched for)</li>
	 * <li>Update statement (the values to be changed)</li>
	 * <li>Delete statement (the values to be searched for)</li>
	 * </ul>
     *
     * @param newColumn The column to be tested
     * @param value The value the column must be equal to
     */
    public void addEqualExpression(DBTableColumnInfo newColumn, Object value);

    /**
     * Adds a column from which data must be returned
     * 
     * This applies to:
	 * <ul>
	 * <li>Select statement</li>
	 * </ul>
	 * 
     * If the table of the column has not been added as a source table,
     * this method must add the table.
     * 
     * If no return columns are added, all data from all source tables
     * must be returned.
     *
     * @param newReturnColumn The column to return data from
     */
    public void addReturnColumn(DBTableColumnInfo newReturnColumn);
    
    /**
     * Adds a primary key value to be used to locate a specific record
     * 
     * This applies to:
	 * <li>Update statement (record to be updated)</li>
	 * <li>Insert statement (primary key value of new record)</li>
	 * <li>Delete statement (record to be removed)</li>
     *
     * @param newColumn The column to be tested
     * @param value The value the column must be equal to
     */
    public void setPrimaryKeyValue(Object pKey);
    /**
     * When jLips is done with the statement, this method is called.
     * 
     * The statement implementation can use this method to do any cleanup
     * after the statement has been used.
     * 
     * After this method is called, jLips will not use the statement object
     * any more.
     */
    public void release();

    /**
     * Must execute a select statement on the database.
     * 
     * The implementation should build a select statement with the provided
     * data and return the ResultSet
     * 
     * <h2>Using <code>doSelect</code></h2>
     * DOCUMENT ME!
     *
     * @return ResultSet with the data returned from the database
     * @throws SQLException Exception thrown by the database driver
     * @throws DBBadPropertiesException if the properties are not correct
     * or sufficent to execute a select statement
     */
    public ResultSet doSelect() throws SQLException, DBBadPropertiesException;

    public ResultSet doUpdate() throws SQLException, DBBadPropertiesException;

    public ResultSet doInsert() throws SQLException, DBBadPropertiesException;

    public ResultSet doDelete() throws SQLException, DBBadPropertiesException;

    /**
     * @see java.lang.Object#toString()
     */
    public String toString();
    
    public static final String DBS_SELECT_STM = "select";
    public static final String DBS_UPDATE_STM = "update";
    public static final String DBS_INSERT_STM = "insert";
    public static final String DBS_DELETE_STM = "delete";
    public void printStatement(String type) throws DBBadPropertiesException;
}