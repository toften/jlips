/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * DBSimpleProperties.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: DBSimpleProperties.java,v $
 * Revision 1.1  2004/12/07 10:48:42  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist.db;

/**
 * DOCUMENT ME!
 *
 * @author thomas
 *
 */
public class DBSimpleProperties extends DBAbstractProperties implements
        DBProperties {

    /**
     * Constructor for DBSimpleProperties
     * 
     * DOCUMENT ME!
     * 
     * @param driver
     * @param url
     * @param username
     * @param password
     * @param schema
     */
    public DBSimpleProperties(String driver, String url, String username,
            String password, String schema) {
        super(driver, url, username, password, schema);
        // TODO Auto-generated constructor stub
    }
}
