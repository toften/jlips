/*
 * General JavaScript stuff
 *
 *
 * The CSS classes used are:
 * Table: menu
 *
 * $Log: general.js,v $
 *
 */

function insert_logos() {
		document.write("<table class='logos'><tr><td><hr /></tr></td>");
    
    for (i = 0; i < logos.length; i++) {
  		document.write("<tr><td class='logolink'>");
  		document.write("<a href='" + logos[i][0] + "'>");
  		document.write("<img border='0' alt='" + logos[i][1] + "' title='" + logos[i][1] + "' ");
      document.write("src='" + logos[i][2] + "'/>");
  		document.write("</a></tr></td>");
    }
    document.write("</table>");
}


function decode_cvs_date(cvs_date) {
    s = new String(cvs_date);
    
    return(s.substr(7,10));
}


function show_menu(items, cssname) {
	/*
	 * Dechiper the URL to determine what page we are on
	 */
	ci 					= 0;			     // Holds the index number of the current menu item

	/*
     * Find the index of the current menu item
     */
    docURL 			    = document.URL;
    ul 					= docURL.length;    // Length of the URL
	ll 					= 0;				// Local holder of the length of the menu item
    for (i = 0;i < items.length; i++) {
        if (docURL.substring(ul - (items[i][0].length), ul).replace(/\\/i,"/",1) == items[i][0]) {
            if (items[i][0].length > ll) {
			    ci = i;
				ll = items[i][0].length;
			}
		}
	}
    
    level = find_slashes(items[ci][0]);

    if (level == 0)
        ls = "./"
    else
    {
      ls = "";
      
      for (i = 0;i < level;i++)
          ls += "../"
    }

		
	/*
	 * Print out the menu items
	 */
		document.write("<table class='" + cssname + "'>");

    for (i = 0;i < items.length; i++) {
	    document.write("<tr class='" + cssname + "'><td class='" + cssname + find_slashes(items[i][0]));
		// If the item is the currently selected one...
        if (i == ci) {
     	    document.write("selected'>");
            document.write(items[i][1]);        
		}
        else {
            document.write("'><a class='" + cssname + "' href='");
						if (items[i][0].substring(0, 4) != "http")
							 document.write(ls);
							 
            document.write(items[i][0] + "'>" + items[i][1] + "</a>");
		}
	    document.write("</td></tr>");        
    }
						
	document.write("</table>");
}

function menu() {
		show_menu(main_menu_items, "menu-item");
}

function find_slashes(text) {
    slash_count = 0;

    if (text.substring(0, 7) != "http://") {
        last_slash = 0;
        while ((last_slash = text.indexOf("/", last_slash) + 1) != 0) {
            slash_count++;
        }
    }
    
    return slash_count;
}
