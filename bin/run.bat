echo off
REM get the name of the project
call .\set_project_name.bat

if "%ROOT%" == "" goto no.root
if "%WORKSPACE%" == "" goto no.workspace
REM we have got both ROOT and WORKSPACE
echo ROOT and WORKSPACE
goto call.root.env.var.batch

:no.workspace
REM We have got ROOT but no WORKSPACE
echo ROOT and no WORKSPACE
set WORKSPACE=%~dp0..\..
:call.root.env.var.batch
if exist %ROOT%\common\bin\set_all_env.bat call %ROOT%\common\bin\set_all_env.bat
goto check_bat

:no.root
if not "%WORKSPACE%" == "" goto no.root.but.workspace
echo no ROOT and no WORKSPACE
set WORKSPACE=%~dp0..\..
goto call.local.batch

:no.root.but.workspace
echo no ROOT and WORKSPACE
:call.local.batch
REM check if we have a common directory in the WORKSPACE
REM this directory will contain various batch files used
REM to set environment variables used by the scripts
if exist %WORKSPACE%\common\set_env.bat call %WORKSPACE%\common\set_env.bat
if exist ..\config\set_local_env.bat call ..\config\set_local_env.bat
REM there is no ROOT, so we MUST run a local batch file
goto no_config


:check_bat
REM Check if we have a common batch file in the ROOT
REM path - if we do, call it...
if not exist %ROOT%\common\bin\%1.bat goto no_config
call %ROOT%\common\bin\%1.bat %2 %3 %4 %5 %6 %7 %8 %9
goto end

REM We havent got one, so we will use local project batch files
:no_config
REM We treat ANT a little specially...
if not "%1" == "ant" goto not_ant_batch

if not "%ANT_HOME%" == "" goto found_ant_home
if "%ant.home%" == "" goto no_ant
set ANT_HOME=%ROOT%\%ant.home%

:found_ant_home
REM This is a local ant build file that MUST be in the same
REM directory as the batch files
call %ANT_HOME%\bin\ant.bat -Dp.name=%project.name% -Dp.local="true" %2 %3 %4 %5 %6 %7 %8 %9
goto end

:not_ant_batch_file
call .\%1.bat %2 %3 %4 %5 %6 %7 %8 %9

:no_ant
echo The variable ANT_HOME has to be defined

:end