/*
 * jlips
 *
 * Copyright (C) 2004 toften.net
 * 
 * PrimaryKeyHandlerFactory.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: PrimaryKeyHandlerFactory.java,v $
 * Revision 1.3  2004/12/07 10:53:32  toften
 * Using the DBConnectionHandler to get database meta data
 *
 * Revision 1.2  2004/11/17 19:57:06  toften
 * Updated documentation
 *
 * Revision 1.1  2004/11/08 23:02:25  toften
 * Initial checkin
 *
 */

package net.toften.jlips.persist.db.pk;

import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.Vector;

import net.toften.jlips.persist.BadDatabaseFormatException;
import net.toften.jlips.persist.db.DBConnectionFactory;
import net.toften.jlips.persist.db.DBTableColumnInfo;
import net.toften.jlips.persist.db.DBTableInfo;

/**
 * This factory is used to manage the primary keys for the records in the
 * database
 * 
 * Each record must have a primary key field. The primary key must be defined
 * in the database when the table is created.
 * The SQL for this could be:
 * <code>
 * CREATE TABLE person
 * (
 *   id           INTEGER        ,
 *   name         VARCHAR(40)    ,
 *   birthday     DATE           ,
 *   <b>PRIMARY KEY(id)</b>
 * );
 * </code>
 * 
 * Each table should only have <i>one</i> primary key field.
 * 
 * The factory will create a handler for the primary key of each table.
 * This is done when the {@link net.toften.jlips.persist.TableHandler} is 
 * created.
 * 
 * @author thomas
 * @see net.toften.jlips.persist.db.pk.PrimaryKeyHandler
 */
public final class PrimaryKeyHandlerFactory {
    
    /**
     * Private constructor to prevent instantiation of
     * this class
     */
    private PrimaryKeyHandlerFactory() {
        
    }
    
    /**
     * Return a {@link PrimaryKeyHandler} for the specified table
     * 
     * Given a table, the factory will create a primary key handler
     * and return it. The created key handler will deal with whatever
     * type of primary key and this will be transparent to the TableHandler
     *
     * @param tableInfo The table info
     * @return The created {@link PrimaryKeyHandler}
     * @throws BadDatabaseFormatException If the table has no defined key or the
     * data type of the primary key is not supported
     * @throws SQLException
     */
    public static PrimaryKeyHandler getPrimaryKeyHandler(DBTableInfo tableInfo) 
    throws BadDatabaseFormatException, SQLException {
        PrimaryKeyHandler returnPKH = null;
        
        DBTableColumnInfo primaryKeyColumnInfo = DBConnectionFactory.getPrimaryKeyField(tableInfo);
        
        switch (primaryKeyColumnInfo.getSqlType()) {
        case Types.INTEGER:
            returnPKH = new IntegerPrimaryKeyHandler(tableInfo, primaryKeyColumnInfo);
            break;

        default:
            throw new BadDatabaseFormatException("Primary key type is unsupported");
        }
        
        return returnPKH;
    }
}
