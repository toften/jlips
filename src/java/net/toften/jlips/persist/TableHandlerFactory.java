/*
 * (C) 2003 toften.net
 *
 * TableHandlerFactory.java
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *
 * Used code formatting convention: Sun
 *
 * Created by: thomas
 *
 * File change log:
 * ----------------
 * $Log: TableHandlerFactory.java,v $
 * Revision 1.8  2005/02/13 17:18:07  toften
 * Added DatabaseHandler layer
 *
 * Revision 1.7  2004/12/07 10:51:38  toften
 * Persist interfaces are now mapped to tables
 *
 * Revision 1.6  2004/11/17 23:44:03  toften
 * Updated documentation
 *
 * Revision 1.5  2004/11/08 23:02:04  toften
 * Changed the initialisation of the TableHandlers, so they are now directly initialised with the tableinfo, entity class and primary key handler
 *
 * Revision 1.4  2004/10/30 12:24:19  toften
 * Initial checkin
 *
 * Revision 1.3  2004/01/14 19:21:30  toften
 * Various changes to make the code more coherent. Also much revised comments
 *
 * Revision 1.2  2003/01/07 23:24:35  toften
 * Updated various comments.
 * Added more support for Collections
 *
 * Revision 1.1  2002/12/16 14:48:09  toften
 * Initial checkin
 *
 *
 */

package net.toften.jlips.persist;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import net.toften.jlips.persist.TableHandler.ColumnHandler;
import net.toften.jlips.persist.db.DBConnectionFactory;
import net.toften.jlips.persist.db.DBTableColumnInfo;
import net.toften.jlips.persist.db.DBTableInfo;
import net.toften.jlips.persist.db.pk.PrimaryKeyHandler;
import net.toften.jlips.persist.db.pk.PrimaryKeyHandlerFactory;


/**
 * The TableHandlerFactory is used to generate the TableHandler classes for the
 * individual tables.
 * 
 * This class is only used when jLips is initialised
 *
 * @author thomas
 * @version $Revision: 1.8 $
 */
final class TableHandlerFactory {
    /**
     * Holds the table handler
     * 
     * The <b>key</b> is the name of the table in lower case
     * and the <b>value</b> is the TableHandler object
     */
    private static HashMap entityHandlers = new HashMap();

    /**
     * Private constructor to prevent instantiation of
     * this class
     */
    private TableHandlerFactory() {
        
    }
    
    /**
     * Initialises the TableHandlers
     * 
     * The init method will analyse the
     * meta-data from the database, using the following information:
     * <ul>
     * <li>Table names</li>
     * <li>Column names</li>
     * <li>Primary key names</li>
     * </ul>
     * 
     * Using this infomation it will create a TableHandler object for each table in
     * the database, mapping to the TableHandler object in the handlers
     * HashMap object.  All the tables in the database are read before any of
     * the mapping  between the tables and the TableHandler objects. The
     * reason for this is the need to know the existense of tables that take
     * part in  relationships.   It is important that the naming conventions
     * of the tables and columns are followed for the init method to correctly
     * read the database meta-data.  Note that this method is package scoped,
     * so it should only be invoked from within the package.
     *
     * @param tableMap A map containing a list of the table names and the names
     *        of the equivalent interfaces that is to be used to access the
     *        records of the table
     *
     * @throws ClassNotFoundException Thrown if the database factory can not load properties classes
     * @throws IllegalAccessException {@link Class#forName(java.lang.String)
     * @throws InstantiationException {@link Class#forName(java.lang.String)
     * @throws SQLException
     *
     * @see TableHandler
     * @see EntityFactory#init(Properties, HashMap)
     */
    static void init(Map tableMap)
        throws ClassNotFoundException, IllegalAccessException, 
            InstantiationException, BadDatabaseFormatException, SQLException {
        /* First create TableHandler objects for all the tables
         * in the database,...
         */
        
        HashMap handlers = new HashMap();
        
        /* Get the table names */
        List tableNames = DBConnectionFactory.getTableNames();
        
        for (Iterator tables = tableNames.iterator(); tables.hasNext();) {
            DBTableInfo tableInfo = (DBTableInfo) tables.next();
            
            /* If the handler for the table has already been created
             * don't create it again!
             */
            if (!handlers.containsKey(tableInfo.getTableName())) {
	            /*	If we need to map the table, i.e. it is in the table map
	            	This means we will only create table handlers for tables that has
	            	been specified in the table map. This means all system tables etc.
	            	will not have a table map and therefore can not be accessed through
	            	jLips
	            	*/
	            if (tableMap.containsValue(tableInfo.getTableName())) {
	                // Get the Primary Key Handler
	                PrimaryKeyHandler PKH = PrimaryKeyHandlerFactory.getPrimaryKeyHandler(tableInfo);
	
	                // Create the TableHandler
	                TableHandler th = new TableHandler(tableInfo, PKH);	// New table handler
	                
	                // Setup the Column Handlers
	                List fieldNames = DBConnectionFactory.getTableFields(tableInfo);
	
	                for (Iterator iter = fieldNames.iterator(); iter.hasNext();) {
                        DBTableColumnInfo element = (DBTableColumnInfo) iter.next();
                        
	                    ColumnHandler ch             = th.new ColumnHandler(element);	// New column handler
	                }
	
	                handlers.put(tableInfo.getTableName().toLowerCase(), th); // put the handler in the map
	            }
	        }
        }
        
        /*
         * Setting up the links between the persist interfaces and the table handlers
         */
        Set persistInterfaces = tableMap.keySet();
        
        for (Iterator pInterfaces = persistInterfaces.iterator(); pInterfaces.hasNext();) {
            String interfaceName = (String) pInterfaces.next();
            
            Class persistClass = null;
        	            
            /* Load the interface */
            try {
                persistClass = Class.forName(interfaceName);
                
                // Test that the persistClass implements the PersistInterface interface
                if (!TestForInterfaceImplementation(persistClass, PersistEntity.class))
                    throw new BadDatabaseFormatException(interfaceName + " does NOT implement the PersistEntity interface!");
                
                entityHandlers.put(persistClass, handlers.get(((String)tableMap.get(interfaceName)).toLowerCase()));
            } catch (ClassNotFoundException e) {
                throw new BadDatabaseFormatException("Can not load interface " + interfaceName + " for table ");
            }	                          
        }
    }

    /**
     * Returns a TableHandler object given the class of the persistence
     * interface
     *
     * @param theEntity the persist entity that a table handler is needed for
     *
     * @return the table handler for the persist entity
     */
    static DatabaseHandler getHandler(Class theEntity) {
        return (DatabaseHandler)entityHandlers.get(theEntity);
    }
    
    /**
     * When only a name of a persist entity is avaliable, this method is used
     * 
     * The method will go through all the available persist entities and find 
     * one that matches with the name.
     * 
     * For example if you search for an entity called <code>Address</code>
     * and the available persist entities are:
     * <pre>
     * net.toften.jlips.demo.HomeAddress
     * net.toften.jlips.demo.Person
     * net.toften.jlips.demo.Address
     * </pre>
     * 
     * Then the class of the <code>net.toften.jlips.demo.Address</code> is
     * returned.
     * 
     * If the entity is not found, <code>null</code> is returned.
     *
     * @param name String with persist entity name
     * @return the class of the entity
     */
    static Class getEntity(String name) {
        Set entities = entityHandlers.keySet();
        Class returnEntity = null;
        
        for (Iterator eIter = entities.iterator(); eIter.hasNext();) {
            Class entity = (Class) eIter.next();
            
            if (entity.getSimpleName().toLowerCase().equals(name)) {
                returnEntity = entity;
                break;
            }
        }
        
        return returnEntity;
    }
    
    /**
     * This method tests if an interface implements another interface somewhere in
     * the inheritance hierarchy
     *
     * @param testInterface The interface that must be tested
     * @param impl The interface that it must implement
     * @return true if testInterface implements impl
     */
    private static boolean TestForInterfaceImplementation(Class testInterface, Class impl) {
        boolean returnValue = false;
        
        Class[] implementedInterfaces = testInterface.getInterfaces();
        for (int i = 0; i < implementedInterfaces.length; i++) {
            //System.out.println(testInterface.getName() + ":" + implementedInterfaces[i].getName());
            
            if (implementedInterfaces[i].equals(impl)) {
                returnValue = true;
                break;
            }
            returnValue = TestForInterfaceImplementation(implementedInterfaces[i], impl);
        }
        
        return returnValue;
    }
}
